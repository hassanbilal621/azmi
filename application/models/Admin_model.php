<?php
class Admin_model extends CI_Model
{

	public function login($email, $password)
	{
		$this->db->where('email', $email);
		$result = $this->db->get('admin');
		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;
			if (password_verify($password, $hash)) {
				return $result->row(0)->admin_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	// Product / Add / Update / Delete start.....

	public function get_products()
	{

		$this->db->join('category', 'category.category_id = product.product_cat_id', 'left');
		$this->db->join('suppliers', 'suppliers.suppliers_id = product.product_suppliers_id', 'left');

		$query = $this->db->get('product');
		return $query->result_array();
	}
	public function total_product()
	{
		$query = $this->db->get('product');
		return $query->num_rows();
	}

	public function total_supplier_product($supplierid)
	{
		$this->db->where('product_suppliers_id', $supplierid);
		$query = $this->db->get('product');
		return $query->num_rows();
	}

	public function add_product($imgname)
	{
		$data = array(

			'product' => $this->input->post('product'),
			'product_suppliers_id' => $this->input->post('product_suppliers_id'),
			'product_cat_id' => $this->input->post('product_cat_id'),
			'pro_img' => $imgname,
			'market_price' => $this->input->post('marketprice'),
			'trade_price' => $this->input->post('tradeprice'),
			'pack_size' => $this->input->post('packsize'),
			'general_name' => $this->input->post('generalname'),
			'discount' => $this->input->post('discount'),
			'date' => $this->input->post('date')


		);

		$this->security->xss_clean($data);
		$this->db->insert('product', $data);
	}

	public function updateproduct($imgname,  $productid)
	{
		$data = array(

			'product' => $this->input->post('product'),
			'product_suppliers_id' => $this->input->post('product_suppliers_id'),
			'product_cat_id' => $this->input->post('product_cat_id'),
			'pro_img' => $imgname,
			'market_price' => $this->input->post('marketprice'),
			'trade_price' => $this->input->post('tradeprice'),
			'pack_size' => $this->input->post('packsize'),
			'general_name' => $this->input->post('generalname'),
			'discount' => $this->input->post('discount')

		);

		$this->security->xss_clean($data);
		$this->db->where('product_id', $productid);
		$this->db->update('product', $data);
	}

	public function update_product_stock($productid, $total_stock)
	{
		$data = array(

			'product_stock' => $total_stock

		);

		$this->security->xss_clean($data);
		$this->db->where('product_id', $productid);
		$this->db->update('product', $data);
	}

	public function deleteproduct($productid)
	{
		$this->db->where('product_id', $productid);
		$this->db->delete('product');
	}

	// Product / Add / Update / Delete  End

	// Category / Add / Update / Delete  start

	public function add_cat()
	{
		$data = array(
			'category' => $this->input->post('category'),

		);

		$this->security->xss_clean($data);
		$this->db->insert('category', $data);
	}

	public function get_cat()
	{
		$query = $this->db->get('category');
		return $query->result_array();
	}

	// Category / Add / Update / Delete  End

	// suppliers / Add / Update / Delete  start

	public function addsuppliers($imgname)
	{
		$data = array(
			'suppliers' => $this->input->post('suppliers'),
			'email' => $this->input->post('email'),
			'supplier_img' => $imgname,
			'address' => $this->input->post('address'),
			'supplier_date'  => date('d/m/Y')
		);

		$this->security->xss_clean($data);
		$this->db->insert('suppliers', $data);
	}

	public function supplier_disable($supplierid)
	{
		$data = array(
			'status' => 'deactive'
		);

		$this->security->xss_clean($data);
		$this->db->where('suppliers_id', $supplierid);
		$this->db->update('suppliers', $data);
	}
	public function supplier_enable($supplierid)
	{
		$data = array(
			'status' => 'active'
		);

		$this->security->xss_clean($data);
		$this->db->where('suppliers_id', $supplierid);
		$this->db->update('suppliers', $data);
	}

	public function get_suppliers()
	{

		$query = $this->db->get('suppliers');
		return $query->result_array();
	}

	public function get_supplier($supplierid)
	{


		$this->db->where('suppliers_id', $supplierid);
		$query = $this->db->get('suppliers');
		return $query->row_array();
	}
	public function total_supplier()
	{
		$query = $this->db->get('suppliers');
		return $query->num_rows();
	}
	public function updatesupplier($imgname,  $supplierid)
	{
		$data = array(
			'suppliers' => $this->input->post('suppliers'),
			'email' => $this->input->post('email'),
			'supplier_img' => $imgname,
			'address' => $this->input->post('address'),
		);

		$this->security->xss_clean($data);
		$this->db->where('suppliers_id', $supplierid);
		$this->db->update('suppliers', $data);
	}

	public function del_suppliers($supplierid)
	{
		$this->db->where('suppliers_id', $supplierid);
		$this->db->delete('suppliers');
	}


	// suppliers / Add / Update / Delete  End

	// Purchase Order / Add / Update / Delete  start

	public function add_purchase_order()
	{
		$data = array(
			'date' => $this->input->post('date'),
			'order_by' => $this->input->post('order_by'),
			'order_date' => $this->input->post('order_date'),
			'supplier_name_id' => $this->input->post('supplier_name_id'),
			'order_note' => $this->input->post('order_note'),
			'grand_total' => $this->input->post('total_amount'),
			'order_status' => 'pending',
			'date' => date('d/m/Y')

		);
		$this->security->xss_clean($data);
		$this->db->insert('purchase_order', $data);

		return $this->db->insert_id();
	}

	public function update_purchaseorder($total_paid, $purchaseorderid)
	{
		$data = array(
			'paid_amount' => $total_paid

		);
		$this->security->xss_clean($data);
		$this->db->where('purchase_order_id', $purchaseorderid);
		$this->db->update('purchase_order', $data);
	}
	public function get_purchase_order()
	{
		$this->db->join('suppliers', 'purchase_order.supplier_name_id = suppliers.suppliers_id', 'left');

		$query = $this->db->get('purchase_order');
		return $query->result_array();
	}

	public function total_purchase_order()
	{
		$query = $this->db->get('purchase_order');
		return $query->num_rows();
	}


	public function get_purchaseorder($purchaseorderid)
	{
		$this->db->join('suppliers', 'purchase_order.supplier_name_id = suppliers.suppliers_id', 'left');

		$this->db->where('purchase_order.purchase_order_id', $purchaseorderid);
		$query = $this->db->get('purchase_order');
		return $query->row_array();
	}
	public function get_purchaseorderitems($purchaseorderid)
	{
		$this->db->join('product', 'purchase_order_item.product_name_id = product.product_id', 'left');
		$this->db->where('purchase_order_id', $purchaseorderid);
		$query = $this->db->get('purchase_order_item');
		return $query->result_array();
	}

	public function get_pending_order()
	{
		$this->db->join('suppliers', 'purchase_order.supplier_name_id = suppliers.suppliers_id', 'left');

		$this->db->where('purchase_order.order_status', 'pending');
		$query = $this->db->get('purchase_order');
		return $query->result_array();
	}

	public function update_pending_order($purchaseorderid)
	{
		$data = array(
			'order_status' => 'pending'

		);
		$this->security->xss_clean($data);
		$this->db->where('purchase_order_id', $purchaseorderid);
		$this->db->update('purchase_order', $data);
	}

	public function get_recived_order()
	{
		$this->db->join('suppliers', 'purchase_order.supplier_name_id = suppliers.suppliers_id', 'left');

		$this->db->where('purchase_order.order_status', 'recived');
		$query = $this->db->get('purchase_order');
		return $query->result_array();
	}

	public function update_recived_order($purchaseorderid)
	{
		$data = array(
			'order_status' => 'recived'

		);
		$this->security->xss_clean($data);
		$this->db->where('purchase_order_id', $purchaseorderid);
		$this->db->update('purchase_order', $data);
	}

	public function get_cancel_order()
	{
		$this->db->join('suppliers', 'purchase_order.supplier_name_id = suppliers.suppliers_id', 'left');

		$this->db->where('purchase_order.order_status', 'cancel');
		$query = $this->db->get('purchase_order');
		return $query->result_array();
	}


	public function update_cancel_order($purchaseorderid)
	{
		$data = array(
			'order_status' => 'cancel'

		);
		$this->security->xss_clean($data);
		$this->db->where('purchase_order_id', $purchaseorderid);
		$this->db->update('purchase_order', $data);
	}

	// Purchase Order / Add / Update / Delete  End

	// invoice / Add / Update / Delete  start

	public function addinvoice()
	{
		$data = array(
			'invoicedate' => $this->input->post('invoicedate'),
			'customer_id' => $this->input->post('customer_id'),
			'order_by' => $this->input->post('order_by'),
			'companyid' => $this->input->post('companyid'),
			'order_note' => $this->input->post('order_note'),
			'grand_total' => $this->input->post('total_amount'),
			'invoice_status' => 'unpaid',

		);
		$this->security->xss_clean($data);
		$this->db->insert('invoice', $data);

		return $this->db->insert_id();
	}

	public function get_invoices()
	{
		$this->db->join('customer', 'invoice.customer_id = customer.customer_id', 'left');
		$this->db->join('company', 'invoice.companyid = company.company_id', 'left');

		$query = $this->db->get('invoice');
		return $query->result_array();
	}


	public function total_invoices()
	{
		$query = $this->db->get('invoice');
		return $query->num_rows();
	}

	public function total_company_invoices($companyid)
	{
		$this->db->where('companyid', $companyid);
		$query = $this->db->get('invoice');
		return $query->num_rows();
	}

	public function get_invoice($invoiceid)
	{
		$this->db->join('customer', 'invoice.customer_id = customer.customer_id', 'left');
		$this->db->join('company', 'invoice.companyid = company.company_id', 'left');

		$this->db->where('invoice_id', $invoiceid);
		$query = $this->db->get('invoice');
		return $query->row_array();
	}

	public function get_company_invoice($companyid)
	{
		$this->db->join('customer', 'invoice.customer_id = customer.customer_id', 'left');
		$this->db->join('company', 'invoice.companyid = company.company_id', 'left');

		$this->db->where('companyid', $companyid);
		$query = $this->db->get('invoice');
		return $query->result_array();
	}


	public function get_invoiceItem($invoiceid)
	{

		$this->db->join('stock', 'invoice_item.stockid = stock.stock_id', 'left');
		$this->db->where('invoice_id', $invoiceid);
		$query = $this->db->get('invoice_item');
		return $query->result_array();
	}

	public function update_invoice($imgname)
	{
		$data = array(
			'invoice' => $this->input->post('invoice'),
			'email' => $this->input->post('email'),
			'pro_img' => $imgname,
			'address' => $this->input->post('address'),
			'invoice_status' => 'unpaid',
		);

		$this->security->xss_clean($data);
		$this->db->where('invoice_id', $this->input->post('editsupplierid'));
		$this->db->update('invoice', $data);
	}

	public function del_invoice($invoiceid)
	{
		$this->db->where('invoice_id', $invoiceid);
		$this->db->delete('invoice');
	}


	// invoice / Add / Update / Delete  End

	// Company / Add / Update / Delete  start

	public function updatecompany()
	{
		$data = array(
			'companyname' => $this->input->post('companyname'),
			'companyaddress' => $this->input->post('address'),
			'status' => $this->input->post('status')


		);
		$this->security->xss_clean($data);
		$this->db->where('company_id', $this->input->post('companyid'));
		$this->db->update('company', $data);
	}


	public function get_companies()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('company');
		return $query->result_array();
	}
	public function get_companies_all()
	{
		$query = $this->db->get('company');
		return $query->result_array();
	}

	public function get_company($companyid)
	{

		$this->db->where('company_id', $companyid);
		$query = $this->db->get('company');
		return $query->row_array();
	}

	public function company_disable($companyid)
	{
		$data = array(
			'status' => 'deactive'
		);
		$this->security->xss_clean($data);
		$this->db->where('company_id', $companyid);
		$this->db->update('company', $data);
	}
	public function company_enable($companyid)
	{
		$data = array(
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		$this->db->where('company_id', $companyid);
		$this->db->update('company', $data);
	}


	// Company / Add / Update / Delete  End

	// stock / Add / Update / Delete  start

	public function addstock($productid)
	{
		$data = array(
			'stock_date' => $this->input->post('stock_date'),
			'stock_product_id' => $productid,
			'batch_no' => $this->input->post('batch_no'),
			'purchaseorderid' => $this->input->post('purchaseorderid'),
			'new_stock' => $this->input->post('new_stock'),
			'manufacturing_date' => $this->input->post('manufacturing_date'),
			'expiredate' => $this->input->post('expiredate'),
		);

		$this->security->xss_clean($data);
		$this->db->insert('stock', $data);
	}

	public function get_purchase_order_product()
	{

		if (isset($_GET['purchase_order_id'])) {

			$this->db->where('purchase_order_id', $_GET['purchase_order_id']);
			$result = $this->db->get('purchase_order');

			return $result->row_array();
		}
	}

	public function get_stocks()
	{
		$query = $this->db->get('stock');
		return $query->result_array();
	}
	public function get_stocks_product()
	{
		$this->db->join('product', 'stock.stock_product_id = product.product_id', 'left');

		$query = $this->db->get('stock');
		return $query->result_array();
	}
	public function getstock()
	{

		if (isset($_GET['product_id'])) {

			$this->db->where('stock_product_id', $_GET['product_id']);
		}

		$result = $this->db->get('stock');

		return $result->result_array();
	}


	public function get_ajax_stock($stockid)
	{

		$this->db->where('stock_id', $stockid);
		$query = $this->db->get('stock');
		return $query->row_array();
	}

	public function total_stock()
	{
		$query = $this->db->get('stock');
		return $query->num_rows();
	}

	public function updatestock($stockid)
	{
		$data = array(
			'stock_date' => $this->input->post('stock_date'),
			'stock_product_id' => $this->input->post('stock_product_id'),
			'batch_no' => $this->input->post('batch_no'),
			'purchaseorderid' => $this->input->post('purchaseorderid'),
			'new_stock' => $this->input->post('new_stock'),
			'manufacturing_date' => $this->input->post('manufacturing_date'),
			'expiredate' => $this->input->post('expiredate'),
		);

		$this->security->xss_clean($data);
		$this->db->where('stock_id', $stockid);
		$this->db->update('stock', $data);
	}

	public function update_usedstock($used_stock, $stockid)
	{
		$data = array(
			'used_stock' => $used_stock

		);

		$this->security->xss_clean($data);
		$this->db->where('stock_id', $stockid);
		$this->db->update('stock', $data);
	}
	public function del_stock($stockid)
	{
		$this->db->where('stock_id', $stockid);
		$this->db->delete('stock');
	}


	// stock / Add / Update / Delete  End

	// customer / Add / Update / Delete  start

	public function addcustomer()
	{
		$data = array(
			'customer_name' => $this->input->post('customer_name'),
			'billing_address' => $this->input->post('billing_address'),
		);

		$this->security->xss_clean($data);
		$this->db->insert('customer', $data);
	}

	public function get_customer()
	{

		$query = $this->db->get('customer');
		return $query->result_array();
	}


	public function get_customer_acive()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('customer');
		return $query->num_rows();
	}
	public function get_customer_deactive()
	{
		$this->db->where('status', 'deactive');
		$query = $this->db->get('customer');
		return $query->num_rows();
	}
	public function total_customer()
	{
		$query = $this->db->get('customer');
		return $query->num_rows();
	}
	public function total_customer_active()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('customer');
		return $query->num_rows();
	}
	public function total_customer_deactive()
	{
		$this->db->where('status', 'deactive');
		$query = $this->db->get('customer');
		return $query->num_rows();
	}


	public function updatecustomer()
	{
		$data = array(
			'customer_name' => $this->input->post('customer_name'),
			'billing_address' => $this->input->post('billing_address'),
		);

		$this->security->xss_clean($data);
		$this->db->where('customer_id', $this->input->post('customerid'));
		$this->db->update('customer', $data);
	}

	public function customer_disable($companyid)
	{
		$data = array(
			'status' => 'deactive'
		);
		$this->security->xss_clean($data);
		$this->db->where('customer_id', $companyid);
		$this->db->update('customer', $data);
	}
	public function customer_enable($companyid)
	{
		$data = array(
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		$this->db->where('customer_id', $companyid);
		$this->db->update('customer', $data);
	}
	public function del_customer($customerid)
	{
		$this->db->where('customer_id', $customerid);
		$this->db->delete('customer');
	}


	// customer / Add / Update / Delete  End

	// payment / Add / Update / Delete  start

	public function payment($purchaseorderid)
	{
		$data = array(
			'payment' => $this->input->post('payment'),
			'purchass_order_id_payment' => $purchaseorderid,
			'date' => date('Y-m-d H:i:s'),
			'paymentdate' => $this->input->post('paymentdate'),
			'paymentnote' => $this->input->post('paymentnote')

		);

		$this->security->xss_clean($data);
		$this->db->insert('payment', $data);
	}

	public function get_payments()
	{
		$query = $this->db->get('payment');
		return $query->result_array();
	}
	public function	get_payment($paymentid)
	{
		$this->db->where('payment_id', $paymentid);
		$query = $this->db->get('payment');
		return $query->row_array();
	}
	public function update_payment($paymentid)
	{
		$data = array(
			'payment' => $this->input->post('payment'),
			'paymentdate' => $this->input->post('paymentdate'),
			'paymentnote' => $this->input->post('paymentnote')
		);

		$this->security->xss_clean($data);
		$this->db->where('payment_id', $paymentid);
		$this->db->update('payment', $data);
	}

	public function del_payment($paymentid)
	{
		$this->db->where('payment_id', $paymentid);
		$this->db->delete('payment');
	}


	// payment / Add / Update / Delete  End

	// admin / Add / Update / Delete Start 

	public function get_admininfo($admin_id)
	{
		$this->db->where('id', $admin_id);
		$result = $this->db->get('admin');

		return $result->row_array();
	}

	public function get_admins()
	{
		$this->db->order_by('admin.id', 'DESC');
		$query = $this->db->get('admin');
		return $query->result_array();
	}


	public function get_stock_by_poductid($productid)
	{

		$this->db->where('stock_product_id', $productid);
		$this->db->order_by('stock.stock_id', 'DESC');
		$query = $this->db->get('stock');
		return $query->result_array();
	}


	public function add_admin($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
		);

		$this->security->xss_clean($data);
		$this->db->insert('admin', $data);
	}

	public function update_admin($admin_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password
		);

		$this->security->xss_clean($data);
		$this->db->where('id', $admin_id);
		$this->db->update('admin', $data);
	}

	public function del_admin($adminid)
	{
		$this->db->where('id', $adminid);
		$this->db->delete('admin');
	}

	// admin / Add / Update / Delete Start 

	// expense_category / Add / Update / Delete  start

	public function addcategory()
	{
		$data = array(
			'category' => $this->input->post('category'),
			'status' => 'deactive'
		);

		$this->security->xss_clean($data);
		$this->db->insert('category', $data);
	}

	public function update_category()
	{
		$data = array(
			'category' => $this->input->post('category'),
		);

		$this->security->xss_clean($data);
		$this->db->where('category_id', $this->input->post('category_id'));
		$this->db->update('category', $data);
	}

	public function get_categories()
	{

		$query = $this->db->get('category');
		return $query->result_array();
	}

	public function get_category($category_id)
	{
		$this->db->where('category_id', $category_id);
		$result = $this->db->get('category');
		return $result->row_array();
	}
	public function get_active_category()
	{

		$this->db->where('status', 'active');
		$query = $this->db->get('category');
		return $query->result_array();
	}

	public function category_delete($category_id)
	{
		$this->db->where('category_id', $category_id);
		$this->db->delete('category');
	}


	public function activecategory($category_id)
	{
		$data = array(
			'status' => 'active'
		);
		$this->db->where('category_id', $category_id);
		$this->db->update('category', $data);
	}

	public function deactivecategory($category_id)
	{
		$data = array(
			'status' => 'deactive'
		);
		$this->db->where('category_id', $category_id);
		$this->db->update('category', $data);
	}

	// category / Add / Update / Delete  End

	// expense_category / Add / Update / Delete  start

	public function addexpensecategory()
	{
		$data = array(
			'exp_cat' => $this->input->post('exp_cat'),
			'status' => 'deactive'
		);

		$this->security->xss_clean($data);
		$this->db->insert('expensecat', $data);
	}

	public function update_exp_cat()
	{
		$data = array(
			'exp_cat' => $this->input->post('exp_cat'),
		);

		$this->security->xss_clean($data);
		$this->db->where('exp_cat_id', $this->input->post('exp_cat_id'));
		$this->db->update('expensecat', $data);
	}

	public function get_expense_cat()
	{


		$query = $this->db->get('expensecat');
		return $query->result_array();
	}

	public function get_expense_act_cat()
	{

		$this->db->where('status', 'active');
		$query = $this->db->get('expensecat');

		return $query->result_array();
	}

	public function cat_delete($expenseid)
	{
		$this->db->where('exp_cat_id', $expenseid);
		$this->db->delete('expensecat');
	}


	public function activestatus($expenseid)
	{
		$data = array(
			'status' => 'active'
		);
		$this->db->where('exp_cat_id', $expenseid);
		$this->db->update('expensecat', $data);
	}

	public function deactivestatus($expenseid)
	{
		$data = array(
			'status' => 'deactive'
		);
		$this->db->where('exp_cat_id', $expenseid);
		$this->db->update('expensecat', $data);
	}

	// expense_category / Add / Update / Delete  End

	// expense / Add / Update / Delete  start	
	public function addexpense()
	{
		$data = array(
			'expense_cat_id' => $this->input->post('expense_cat_id'),
			'expense_price' => $this->input->post('expense_price'),
			'expense_note' => $this->input->post('expense_note'),
			'expense_date' => $this->input->post('expense_date')

		);

		$this->security->xss_clean($data);
		$this->db->insert('expense', $data);
	}

	public function updateexpense()
	{
		$data = array(
			'expense_cat_id' => $this->input->post('expense_cat_id'),
			'expense_price' => $this->input->post('expense_price'),
			'expense_note' => $this->input->post('expense_note')

		);

		$this->security->xss_clean($data);
		$this->db->where('expense_id', $this->input->post('expenseid'));
		$this->db->update('expense', $data);
	}

	public function get_expense()
	{

		$this->db->join('expensecat', 'expense.expense_cat_id = expensecat.exp_cat_id', 'left');

		$query = $this->db->get('expense');
		return $query->result_array();
	}


	public function update_expense($expenseid)
	{
		$data = array(
			'firstname' => $this->input->post('firstname')
		);

		$this->security->xss_clean($data);
		$this->db->where('expense_id', $expenseid);
		$this->db->update('expense', $data);
	}

	public function expense_delete($expenseid)
	{
		$this->db->where('expense_id', $expenseid);
		$this->db->delete('expense');
	}



	// expense / Add / Update / Delete  End

	// report start

	public function get_sale_report()
	{

		if (isset($_GET['first_date']) && isset($_GET['last_date'])) {

			$first_date = $_GET['first_date'];
			$last_date =  $_GET['last_date'];
			$this->db->join('customer', 'invoice.customer_id = customer.customer_id', 'left');
			$this->db->join('company', 'invoice.companyid = company.company_id', 'left');

			$this->db->where('invoice.invoicedate between "' . $first_date . '" and "' . $last_date . '"');
			$result = $this->db->get('invoice');
			return $result->result_array();
		}
	}
	public function get_expense_report()
	{

		if (isset($_GET['first_date']) && isset($_GET['last_date'])) {

			$first_date = $_GET['first_date'];
			$last_date =  $_GET['last_date'];

			$this->db->where('expense.date between "' . $first_date . '" and "' . $last_date . '"');
			$result = $this->db->get('expense');
			return $result->result_array();
		}
	}


	public function get_Purchase_report()
	{

		if (isset($_GET['first_date']) && isset($_GET['last_date'])) {

			$first_date = $_GET['first_date'];
			$last_date =  $_GET['last_date'];

			$this->db->join('suppliers', 'purchase_order.supplier_name_id = suppliers.suppliers_id', 'left');

			$this->db->where('purchase_order.date between "' . $first_date . '" and "' . $last_date . '"');
			$result = $this->db->get('purchase_order');
			return $result->result_array();
		}
	}
	// report end











	/////******************************AJAx*******************************************//////

	public function get_category_ajax($expenseid)
	{
		$this->db->where('exp_cat_id', $expenseid);
		$result = $this->db->get('expensecat');
		return $result->row_array();
	}


	public function get_ajax_payment($paymentid)
	{
		$this->db->where('payment_id', $paymentid);
		$query = $this->db->get('payment');
		return $query->row_array();
	}

	public function get_expense_ajax($expenseid)
	{
		$this->db->where('expense_id', $expenseid);
		$result = $this->db->get('expense');
		return $result->row_array();
	}

	public function get_product_ajax_details($productid)
	{
		$this->db->where('product_id', $productid);
		$result = $this->db->get('product');
		return $result->row_array();
	}

	public function get_ajax_supplier($supplierid)
	{
		$this->db->where('suppliers_id', $supplierid);
		$result = $this->db->get('suppliers');
		return $result->row_array();
	}

	public function get_ajax_product($productid)
	{
		$this->db->where('product_id', $productid);
		$result = $this->db->get('product');
		return $result->row_array();
	}

	public function get_ajax_purchase_order($purchaseorderid)
	{
		$this->db->where('purchase_order_id', $purchaseorderid);
		$result = $this->db->get('purchase_order');
		return $result->row_array();
	}

	public function get_ajax_company($companyid)
	{
		$this->db->where('company_id', $companyid);
		$result = $this->db->get('company');
		return $result->row_array();
	}

	public function get_customer_ajax($customerid)
	{
		$this->db->where('customer_id', $customerid);
		$result = $this->db->get('customer');
		return $result->row_array();
	}
}
