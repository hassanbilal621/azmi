<div class="row" id="report">

	<?php if (isset($expensess)) {
		?><table id="page-length-option" class="display">
			<thead>
				<tr>
					<th>expense Id</th>
					<th>expense category</th>
					<th>expense Price</th>
					<th>expense Note</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($expensess as $expense) : ?>
					<tr>
						<td><?php echo $expense['expense_id']; ?></td>
						<td><?php echo $expense['exp_cat']; ?></td>
						<td><?php echo $expense['expense_price']; ?></td>
						<td><?php echo $expense['expense_note']; ?></td>
					</tr>
				<?php endforeach; ?>
				</tfoot>

		</table>
	<?php } else {
		echo "No Data Available ";
	} ?>
</div>