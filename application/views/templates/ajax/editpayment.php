<div class="row">
   <div class="col s12">
      <?php echo form_open('admin/managepayment'); ?>
      <div class="row">
         <input type="hidden" name="payment_id" value="<?php echo $payment['payment_id']; ?>" placeholder="0.00" readonly>
      </div>
      <div class="row">
         <div class="input-field col s6">
            <h6>Your Payment Date</h6>
            <input type="text" name="paymentdate" class="datepicker" value="<?php echo $payment['paymentdate']; ?>" placeholder="Type Your Payment Date">
         </div>
         <div class="input-field col s6">
            <h6>Your Payment</h6>
            <input type="number" name="payment" value="<?php echo $payment['payment']; ?>" placeholder="0.00">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s6">
            <h6>Purchass Order id Payment</h6>
            <input type="text" name="purchass_order_id_payment" value="<?php echo  $payment['purchass_order_id_payment']; ?>" readonly>
         </div>
         <div class="input-field col s6">
            <h6>Your Payment Note</h6>
            <input type="text" name="paymentnote" value="<?php echo $payment['paymentnote']; ?>" placeholder="0.00">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s6">
            <button type="submit" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1">Pay
            </button>
         </div>
      </div>
      <?php echo form_close(); ?>
   </div>
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>