<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-alert card " style="background: #262362;">
                <div class="card-content white-text">
                    <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Edit category</h5>
                    </h5>
                </div>
            </div>
            <?php echo form_open('admin/update_category'); ?>
            <div class="input-field col s12">
                <input type="text" name="category" placeholder="Add category" value="<?php echo $category['category']; ?>" required>
                <input type="hidden" name="category_id" value="<?php echo $category['category_id']; ?>">
            </div>
            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
            </button>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</div>
</div>