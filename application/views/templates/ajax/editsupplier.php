<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Edit Supplier</h5>
                        </h5>
                    </div>
                </div>
                <?php echo form_open_multipart('admin/managesuppliers') ?>
                <div class="row">

                    <div class="center input-field col s12">
                        <img class="responsive-img circle z-depth-5" src="<?php echo base_url(); ?>assets/uploads/<?php echo $supplier['supplier_img']; ?>" style="width: 150px;" />
                    </div>
                    <div class="col input-field s6">

                        <div class="file-field">
                            <div class="btn">
                                <span>File</span>
                                <input type="file" name="userfile">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                    </div>
                    <div class="input-field col s6">

                        <label class="active" for="supplier">Supplier Name</label>
                        <input id="suppliers" type="text" value="<?php echo $supplier['suppliers']; ?>" name="suppliers">
                        <input type="hidden" name="supplierid" value="<?php echo $supplier['suppliers_id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <label class="active" for="email">Supplier Email</label>
                        <input id="email" type="text" value="<?php echo $supplier['email']; ?>" name="email">
                    </div>

                    <div class="input-field col s6">
                        <label class="active" for="address">Supplier Address</label>
                        <input id="address" type="text" value="<?php echo $supplier['address']; ?>" name="address">
                    </div>

                    <div class="input-field col s12">
                        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" type="submit" name="action">submit
                            <i class="material-icons right">mode_edit</i>
                        </button>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
</div>