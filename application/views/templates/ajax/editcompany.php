<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Edit Product</h5>
                        </h5>
                    </div>
                </div>
                <?php echo form_open('admin/managecompany') ?>
                <div class="row">
                    <div class="input-field col s12">
                        <label for="name2">Company Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="name2" type="text" value="<?php echo $company['companyname']; ?>" name="companyname">
                        <input type="hidden" name="companyid" value="<?php echo $company['company_id']; ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <label for="address">Company Address</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="address" type="text" value="<?php echo $company['companyaddress']; ?>" name="address">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select  name="status" class="select2 browser-default">
                            <?php if ($company['status'] = 1) {
                            ?>
                                <option selected value="1">Active</option>
                                <option value="2">Deactive</option>
                            <?php
                            } else {
                            ?>
                                <option value="1">Active</option>
                                <option selected value="2">Deactive</option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  right" type="submit" name="action">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close() ?>
    </div>
</div>
</div>