<div class="row" id="productpage">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <table id="page-length-option" class="display">
                        <thead>
                            <tr>
                                <th>Stock Id</th>
                                <th>Stock Date</th>
                                <th>Product Id</th>
                                <th>Purchase Order Id</th>
                                <th>New Stock</th>
                                <th>Used Stock</th>
                                <th>Batch No</th>
                                <th>Manufacturing Date</th>
                                <th>Expire Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stocks as $stock) : ?>
                                <tr>
                                    <td><?php echo $stock['stock_id']; ?></td>
                                    <td><?php echo $stock['stock_date']; ?></td>
                                    <td><?php echo $stock['stock_product_id']; ?></td>
                                    <td><?php echo $stock['purchaseorderid']; ?></td>
                                    <td><?php echo $stock['new_stock']; ?></td>
                                    <td><?php echo $stock['used_stock']; ?></td>
                                    <td><?php echo $stock['batch_no']; ?></td>
                                    <td><?php echo $stock['manufacturing_date']; ?></td>
                                    <td><?php echo $stock['expiredate']; ?></td>
                                    <td>
                                        <button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadstockinfo(this.id)" id="<?php echo $stock['stock_id']; ?>" type="submit" href="#modal3" name="action">EDIT
                                            <i class="material-icons left">edit</i>
                                        </button>
                                        <a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" id="<?php echo $stock['stock_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
                                            <i class="material-icons left">delete_forever</i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col s12">
                        <table>
                            <tr>
                                <?php echo form_open('admin/updatestoks'); ?>
                                <th>Add Deliver Stock</th>
                                <td>
                                    <span>Stock ID</span>
                                    <input type="number" placeholder='Type Your Stock ID' id="stockid" />
                                </td>
                                <td>
                                    <span>Invoice ID</span>
                                    <input type="number" name='stockid' id="stockid" placeholder='Type Your invoice ID' readonly />
                                </td>
                                <td>
                                    <span>Type Your Pay Amount</span>
                                    <input type="number" name="payment" onkeyup="onpay(this.value)" value="" id="pay_amount" placeholder="0.00">
                                </td>
                                <td>
                                    <span>Grand Total Is</span>
                                    <input type="number" name='total_amount' id="total_amount" value="0.00" placeholder='0.00' readonly />
                                </td>
                                <td>
                                    <button type="submit" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1">Pay
                                    </button></td>
                                <?php echo form_close(); ?>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>