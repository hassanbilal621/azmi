<!--editproduct.php-->
<style>
	.bor {
		text-align: center;
		border: 2px #b1a9a9 solid;
		padding: 8px;

	}
</style>
<div class="row">
	<div class="col s12">
		<div class="card">
			<div class="card-content">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Edit Product</h5>
						</h5>
					</div>
				</div>
				<?php echo form_open_multipart('admin/manageproduct'); ?>
				<div class="row">
					<div class="col s12">
						<div class="row">
							<div class="center input-field col s12">
								<img class="responsive-img circle z-depth-5" src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['pro_img']; ?>" style="width: 150px;" />
							</div>
						</div>
						<div class="row">
							<div class="col input-field s6">

								<div class="file-field">
									<div class="btn">
										<span>File</span>
										<input type="file" name="userfile">
									</div>
									<div class="file-path-wrapper">
										<input class="file-path validate" type="text">
									</div>
								</div>

							</div>
							<div class="row">
								<div class="input-field col s6">
									<label class="active" for="name2">Product Name</label>
									<input id="product2" type="text" name="product" value="<?php echo $product['product']; ?>">
									<input type="hidden" name="productid" value="<?php echo $product['product_id']; ?>">
								</div>
								<div class="input-field col s6">
									<label class="active" for="name2">Supplier Name</label>
									<select class="form-control" name="product_suppliers_id" required style="display: list-item;">
										<?php foreach ($suppliers as $supplier) : ?>
											<?php if ($supplier['suppliers_id'] == $product['product_suppliers_id']) {
											?>
												<option selected value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
											<?php
											} else {
											?>
												<option value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
											<?php } ?>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field col s6">
									<label class="active" for="name2">Category Name</label>
									<select class="form-control" name="product_cat_id" required style="display: list-item;">
										<?php foreach ($categories as $category) : ?>
											<?php if ($category['category_id'] == $product['product_cat_id']) {
											?>
												<option selected value="<?php echo $category['category_id']; ?>"><?php echo $category['category']; ?></option>
											<?php
											} else {
											?>
												<option value="<?php echo $category['category_id']; ?>"><?php echo $category['category']; ?></option>
											<?php } ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div id="highlight-table" class="card-default scrollspy">
							<div class="card-content">
								<table class="highlight striped">
									<thead>
										<tr>
											<th class="bor">Market Price</th>
											<th class="bor">Trade Price</th>
											<th class="bor">Pack Size</th>
											<th class="bor">General Name</th>
											<th class="bor">Discount Distributer on %</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="bor">
												<div class="input-field col s12">
													<input id="mktprice" type="number" name="marketprice" value="<?php echo $product['market_price']; ?>">
												</div>
											</td>
											<td class="bor">
												<div class="input-field col s12">
													<input id="trdprice" type="number" name="tradeprice" value="<?php echo $product['trade_price']; ?>">
												</div>
											</td>
											<td class="bor">
												<div class="input-field col s12">
													<input id="packsize" type="number" name="packsize" value="<?php echo $product['pack_size']; ?>">
												</div>
											</td>
											<td class="bor">
												<div class="input-field col s12">
													<input id="name" type="text" name="generalname" value="<?php echo $product['general_name']; ?>">
												</div>
											</td>
											<td class="bor">
												<div class="input-field col s12">
													<input id="discount" type="text" name="discount" value="<?php echo $product['discount']; ?>">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  right" type="submit" name="action">submit
										<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>