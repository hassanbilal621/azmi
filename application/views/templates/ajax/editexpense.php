<div class="row">
    <div class="col s12">
        <div class="card">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Edit Expense</h5>
                        </h5>
                    </div>
                </div>
                  <?php echo form_open('admin/manageexpense'); ?>
                  <div class="input-field col s6">
               
                     <select name="expense_cat_id" class="select2 browser-default">
                        <option disabled selected value="">Select expense category </option>
                        <?php foreach ($categories as $category) : ?>
                        <?php if($expense['expense_cat_id'] == $category['exp_cat_id']) 
                        {
                        ?>
                           <option selected value="<?php echo $category['exp_cat_id']; ?>"><?php echo $category['exp_cat']; ?></option>
                        <?php
                        }
                        else {
                        ?>
                           <option value="<?php echo $category['exp_cat_id']; ?>"><?php echo $category['exp_cat']; ?></option>
                        <?php } 
                        ?>
                        <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="input-field col s6">
                     <input type="number" name="expense_price" placeholder="Add expense Price" value="<?php echo $expense['expense_price']; ?>" required>
                     <input type="hidden" name="expenseid" value="<?php echo $expense['expense_id']; ?>">
                  </div>
                  <div class="input-field col s12">
                     <input type="text" name="expense_note" placeholder="Add expense Note" value="<?php echo $expense['expense_note']; ?>" required>
                  </div>
                  <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                     <i class="material-icons right">send</i>
                  </button>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
      </div>
   </div>
</div>