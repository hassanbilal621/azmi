<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Edit Supplier</h5>
                        </h5>
                    </div>
                </div>
                <?php echo form_open('admin/managecustomer') ?>
                <div class="row">
                    <div class="input-field col s6">
                        <label class="active" for="supplier">Customer Name</label>
                        <input id="customer2" type="text" name="customer_name" value="<?php echo $customer['customer_name']; ?>">
                        <input type="hidden" name="customerid" value="<?php echo $customer['customer_id']; ?>">
                    </div>

                    <div class="input-field col s6">
                        <label class="active" for="email">Billing Address</label>
                        <input id="address2" type="text" name="billing_address" value="<?php echo $customer['billing_address']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" type="submit" name="action">submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>
</div>