    <div class="row">
       <div class="col s12">
          <div class="container">
             <div class="section">
                <div class="card">
                   <div class="card-content">
                      <?php echo form_open('admin/managestock'); ?>
                      <div class="row">
                         <div class="col s10">
                            <h4 class="card-title">Edit Stock</h4>
                         </div>
                         <div class="col s2 right">
                            <input type="text" class="datepicker" name="stock_date" value="<?php echo $stock['stock_date']; ?>" placeholder="Type Date" required>
                            <input type="hidden" name="stock_id" value="<?php echo $stock['stock_id']; ?>" required>
                         </div>
                      </div>
                      <div class="row">
                         <div class="input-field col s6">
                            <select name="stock_product_id" class="select2 browser-default" onchange="product(this.value)">
                               <option disabled selected>Select Product</option>
                               <?php foreach ($products as $product) :
                                    if ($stock['stock_product_id'] == $product['product_id']) {
                                       ?>
                                     <option selected value="<?php echo $product['product_id']; ?>"><?php echo $product['product']; ?></option>
                                  <?php }
                                       ?>

                                  <option value="<?php echo $product['product_id']; ?>"><?php echo $product['product']; ?></option>
                               <?php endforeach ?>
                            </select>
                         </div>

                         <div class="input-field col s6">
                            <input type="number" name="batch_no" placeholder="Type Batch No" value="<?php echo $stock['batch_no']; ?>" required>
                         </div>
                      </div>
                      <div class="row">
                         <div class="input-field col s6">
                            <input type="number" name="purchaseorderid" placeholder="00" value="<?php echo $stock['purchaseorderid']; ?>" required>
                         </div>
                         <div class="input-field col s6">
                            <input type="number" name="new_stock" value="<?php echo $stock['new_stock']; ?>" placeholder="Type Your New stock" required>
                         </div>
                      </div>
                      <div class="row">
                         <div class="input-field col s6">
                            <input type="text" class="datepicker" name="manufacturing_date" value="<?php echo $stock['manufacturing_date']; ?>" placeholder="Type Manufacturing Date" required>
                         </div>
                         <div class="input-field col s6">
                            <input type="text" class="datepicker" name="expiredate" value="<?php echo $stock['expiredate']; ?>" placeholder="Type Expire Date" required>
                         </div>
                      </div>
                      <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1" id="view" type="submit" name="action">Submit
                         <i class="material-icons right">send</i>
                      </button>
                      <?php echo form_close(); ?>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>


    <script>
       $(document).ready(function() {
          $('.datepicker').datepicker();
       });
    </script>
    <script>
       $(document).ready(function() {
          $('.datepicker').datepicker();
       });
    </script>
    <script>
       $(document).ready(function() {
          $('.datepicker').datepicker();
       });
    </script>
    <script>
       function product(productid) {

          $.ajax({
             type: "GET",
             url: "<?php echo base_url(); ?>admin/ajax_get_product_details/" + productid,
             success: function(data) {
                var obj = JSON.parse(data);
                document.getElementById("product_stock").value = obj.product_stock;
             }
          });
       }
    </script>
    <script>
       function addstock() {
          var product_stock = document.getElementById("product_stock").value;
          var newstock = document.getElementById("newstock").value
          var total = Number(product_stock) - Number(newstock);
          document.getElementById("totalstock").value = total;
       }
    </script>