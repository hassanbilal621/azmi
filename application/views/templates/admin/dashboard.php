<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="container">
				<div class="section" id="materialize-sparkline">
					<div class="row">
							<div class="col s6 m4 l2">
								<div class="ct-chart card z-depth-2 border-radius-6">
									<div class="card-content">
										<div class="row">
											<div class="col s12">
												<center><h4 class="card-title">Total Customer</h4> </center>
											</div>
											<div class="col s12 ">
												<center><h6 class="mt-4">
													<?php echo $total_customer; ?>
												</h6> </center>
											</div>
										</div>
									</div>
								</div>
							</div>
						
							<div class="col s6 m4 l2">
								<div class="ct-chart card z-depth-2 border-radius-6">
									<div class="card-content">
										<div class="row">
											<div class="col s12">
												<center><h4 class="card-title">Total Product</h4> </center>
											</div>
											<div class="col s12 ">
												<center><h6 class="mt-4">
													<?php echo $total_product; ?>
												</h6> </center>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s6 m4 l2">
								<div class="ct-chart card z-depth-2 border-radius-6">
									<div class="card-content">
										<div class="row">
											<div class="col s12">
												<center><h4 class="card-title">Total Supplier</h4> </center>
											</div>
											<div class="col s12 ">
												<center><h6 class="mt-4">
													<?php echo $total_supplier; ?>
												</h6> </center>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s6 m4 l2">
								<div class="ct-chart card z-depth-2 border-radius-6">
									<div class="card-content">
										<div class="row">
											<div class="col s12">
												<center><h4 class="card-title">Total Purchase Order</h4> </center>
											</div>
											<div class="col s12 ">
												<center><h6 class="mt-4">
													<?php echo $total_purchase_order; ?>
												</h6> </center>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s6 m4 l2">
								<div class="ct-chart card z-depth-2 border-radius-6">
									<div class="card-content">
										<div class="row">
											<div class="col s12">
												<center><h4 class="card-title">Total Invoicess</h4> </center>
											</div>
											<div class="col s12 ">
												<center><h6 class="mt-4">
													<?php echo $total_invoicess; ?>
												</h6> </center>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s6 m4 l2">
								<div class="ct-chart card z-depth-2 border-radius-6">
									<div class="card-content">
										<div class="row">
											<div class="col s12">
											<center><h4 class="card-title">Total Stock</h4> </center>
											</div>
											<div class="col s12 ">
												<center><h6 class="mt-4">
													<?php echo $total_stock; ?>
												</h6> </center>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
					<div class="row">
						<div class="col s12">
							<div class="ct-chart card z-depth-2 border-radius-6">
								<center><h4 class="heading">Total Supplier</h4> </center>
								<div class="scrollbar scrollbar-morpheus-den">
									<div class="force-overflow">
										<div class="card-content">
											<div class="row">
												<table>
													<thead>
														<tr>
															<th>Supplier Id</th>
															<th>Suppliers</th>
															<th>Total Invoice</th>
															<th>Remaing Amount</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($suppliers as $supplier) : ?>
															<tr>
																<td><?php echo $supplier['suppliers_id']; ?></td>
																<td><?php echo $supplier['suppliers']; ?></td>
																<?php
																	$suplierid = $supplier['suppliers_id'];
																	$supplierinvoice = $this->db->query("SELECT * FROM purchase_order WHERE supplier_name_id=$suplierid");
																	$supplierinvoice1 = $supplierinvoice->num_rows();
																	?>
																<td><?php echo $supplierinvoice1 ?></td>

																<?php

																	$supplierinvoices = $this->db->query("SELECT * FROM purchase_order WHERE supplier_name_id=$suplierid");

																	$supplierinvoices1 = $supplierinvoices->result_array();
																	$totalamount = 0;
																	$paidamount = 0;
																	foreach ($supplierinvoices1 as $invoi) :
																		$totalamount = $totalamount + $invoi['grand_total'];
																		$paidamount = $paidamount + $invoi['paid_amount'];

																	endforeach;
																	$remainingamount = $totalamount - $paidamount;

																	?>


																<td>PKR <?php echo $remainingamount; ?></td>
																<td>
																	<button class="waves-effect waves-light  btn  box-shadow-none border-round mr-1 mb-1 edit modal-trigger" onclick="viewsupplier(this.id)" id="<?php echo $supplier['suppliers_id']; ?>" type="submit" href="#modal3" name="action">View Supplier
																	</button>
																	
																</td>
															</tr>
														<?php endforeach; ?>
														</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12">
							<div class="ct-chart card z-depth-2 border-radius-6">
								<center><h4 class="heading">Total Customer</h4> </center>
								<div class="scrollbar scrollbar-morpheus-den">
									<div class="force-overflow">
										<div class="card-content">
											<div class="row">
												<div class="col s12">

												</div>
												<table>
													<thead>
														<tr>
															<th>Customer Id</th>
															<th>Customer</th>
															<th>Customer Address</th>
															<th>Customer Invoicess</th>
															
														</tr>
													</thead>
													<tbody>
														<?php foreach ($customers as $customer) : ?>
															<tr>
																<td><?php echo $customer['customer_id']; ?></td>
																<td><?php echo $customer['customer_name']; ?></td>
																<td><?php echo $customer['billing_address']; ?></td>
																<?php
																	$customerid = $customer['customer_id'];
																	$customerinvoice = $this->db->query("SELECT * FROM invoice WHERE customer_id=$customerid");
																	$customerinvoice1 = $customerinvoice->num_rows();
																	?>
																<td><?php echo $customerinvoice1 ?></td>
																
																
															</tr>
														<?php endforeach; ?>
														</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal3" class="modal">
	<div class="modal-content">
	</div>
</div>
<script>
	function viewsupplier(supplierid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_view_profile/" + supplierid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>