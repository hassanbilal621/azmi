<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-alert card " style="background: #262362;">
               <div class="card-content white-text">
                  <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Product</h5>
                  </h5>
               </div>
            </div>

            <?php if ($this->session->flashdata('product_submit')) : ?>
               <div class="card-alert card green">
                  <div class="card-content white-text">
                     <span class="card-title white-text darken-1">
                        <i class="material-icons">done</i>Product Register</span>
                     <span class="card-title white-text darken-1">Product Has been Successfully Register .</span>
                  </div>
                  <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                     <span id="closeicon" aria-hidden="true">×</span>
                  </button>
               </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('already')) : ?>
               <div class="card-alert card red">
                  <div class="card-content white-text">
                     <span class="card-title white-text darken-1">
                        <i class="material-icons">error_outline</i> Already Registerd </span>
                     <span class="card-title white-text darken-1">duplicate Product not allow. Plaese Resubmit Your Form with another Product Name Thank You.</span>
                  </div>
                  <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                     <span id="closeicon" aria-hidden="true">×</span>
                  </button>
               </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('field_missing')) : ?>
               <div class="card-alert card red">
                  <div class="card-content white-text">
                     <span class="card-title white-text darken-1">
                        <i class="material-icons">error_outline</i> Missing </span>
                     <span class="card-title white-text darken-1">You Are Missing Some Important Feilds. Plaese Resubmit Your product Thank You.</span>
                  </div>
                  <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                     <span id="closeicon" aria-hidden="true">×</span>
                  </button>
               </div>
            <?php endif; ?>

            <div class="card-content">
               <?php echo form_open_multipart('admin/addproduct'); ?>
               <div class="row">
                  <div class="col s2 right">
                     <label for="date">Date</label>
                     <input type="text" name="date" class="datepicker" required>
                  </div>
               </div>
               <div>
                  <div class="row">
                     <div class="col s6">
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="product2" type="text" name="product" required>
                              <label for="product2">Product Name</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                           <label class="active">Select Suppliers</label>
                              <select class="select2 browser-default" name="product_suppliers_id" required>
                                 <option disabled selected>Select Suppliers</option>
                                 <?php foreach ($suppliers as $supplier) : ?>
                                    <?php if (empty($supplier['suppliers'])) {
                                    } else {
                                    ?>
                                       <option value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
                                    <?php } ?>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label class="active">Select Categories</label>
                              <select class="select2 browser-default" name="product_cat_id" required>
                                 <option disabled selected>Select Categories</option>
                                 <?php foreach ($catagories as $category) : ?>
                                    <?php if (empty($category['category'])) {
                                    } else {
                                    ?>
                                       <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category']; ?></option>
                                    <?php } ?>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col s6">
                        <div class="row">
                           <div id="file-upload" class="section">
                              <div class="row section">
                                 <div class="col s12 m12 l12">
                                    <h6 for="img2">Product Image</h6>
                                    <input type="file" id="input-file-now" class="dropify" data-default-file="" required name="userfile" accept="image/*" />
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <table>
                        <thead>
                           <tr>
                              <th>Market Price</th>
                              <th>Trade Price</th>
                              <th>Pack Size</th>
                              <th>General Name</th>
                              <th>Discount Distributer on %</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>
                                 <div class="input-field col s12">
                                    <input id="mktprice" type="number" name="marketprice" required>
                                    <label for="mktprice">Market Price</label>
                                 </div>
                              </td>
                              <td>
                                 <div class="input-field col s12">
                                    <input id="trdprice" type="number" name="tradeprice" required>
                                    <label for="trdprice">Trade Price</label>
                                 </div>
                              </td>
                              <td>
                                 <div class="input-field col s12">
                                    <input id="packsize" type="number" name="packsize" required>
                                    <label for="packsize">Pack Size</label>
                                 </div>
                              </td>
                              <td>
                                 <div class="input-field col s12">
                                    <input id="name" type="text" name="generalname" required>
                                    <label for="name">General Name</label>
                                 </div>
                              </td>
                              <td>
                                 <div class="input-field col s12">
                                    <input id="discount" type="number" name="discount" min="1" max="100" >
                                    <label for="discount">Discount Distributer(Optional)</label>
                                 </div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">submit
                           <i class="material-icons right">mode_edit</i>
                        </button>
                     </div>
                  </div>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<script>
   setInterval(function() {
      document.getElementById("closeicon").click();
   }, 5000);
</script>