<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Stock</h5>
                        </h5>
                    </div>
                </div>
                <div class="card-content">
                    <div id="my-page" class="row">
                        <?php if ($this->session->flashdata('expense_added')) : ?>
                            <div class="card-alert card green">
                                <div class="card-content white-text">
                                    <span class="card-title white-text darken-1">
                                        <i class="material-icons">done</i>Expense Submit</span>
                                    <span class="card-title white-text darken-1">You are Successfully Submit Your expense.</span>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span id="closeicon" aria-hidden="true">×</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('field_missing')) : ?>
                            <div class="card-alert card red">
                                <div class="card-content white-text">
                                    <span class="card-title white-text darken-1">
                                        <i class="material-icons">error_outline</i>Missing </span>
                                    <span class="card-title white-text darken-1">You Are Missing Some Important Felids. Please Resubmit Your Expense Thank You.</span>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span id="closeicon" aria-hidden="true">×</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php echo form_open('admin/addstock'); ?>
                        <div class="row">
                            <div class="col s10">
                                <h4 class="card-title">Add Stock</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select class="select2 browser-default" id="purchase_order_id" name="stock_po_id">
                                    <option disabled selected>Select Purchase Order Id</option>
                                    <?php foreach ($purchaseorders as $purchaseorder) : ?>
                                        <option value="<?php echo $purchaseorder['purchase_order_id']; ?>"><?php echo $purchaseorder['purchase_order_id']; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="input-field col s6">
                                <a type="submit" onclick="getSearch()" ; name="searchBtn" class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1">Search
                                    <i class="material-icons left">search</i>
                                </a>
                            </div>
                        </div>
                        <div class="row" id="stock_products" style="padding: 0px 15px;">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Batch No</th>
                                        <th>Manufacturing Date</th>
                                        <th>Expire Date</th>
                                        <th>Purchase Order Qty</th>
                                        <th>Remaing Stock</th>
                                        <th>Type New Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="product" type="number" placeholder="Product" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="batch_no" type="number" placeholder="Batch No" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="Manufacturing_date" type="number" placeholder="Manufacturing Date" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="packsize" type="number" placeholder="Expire Date" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="purchase_order_qty" type="text" placeholder="Purchase Order Qty" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="remaing_stock" type="number" placeholder="Remaing Stock" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="type_new_stock" type="number" placeholder="Type New Stock" readonly>
                                            </div>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <button class="waves-effect waves-light btn submit z-depth-2 right mt-1 ml-1" id="view" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    function getSearch() {
        var poid = document.getElementById("purchase_order_id").value;

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/get_search_product",
            data: ({
                purchase_order_id: poid
            }),
            success: function(data) {
                $("#stock_products").html(data);
            }
        });
    }
</script>