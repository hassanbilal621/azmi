<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Expense</h5>
                        </h5>
                    </div>
                </div>
                <div class="card-content">
                    <div id="my-page" class="row">
                        <div class="col s12 m12 l6 4 card-panel border-radius-6 login-card ">
                            <?php if ($this->session->flashdata('expense_added')) : ?>
                                <div class="card-alert card green">
                                    <div class="card-content white-text">
                                        <span class="card-title white-text darken-1">
                                            <i class="material-icons">done</i>Expense Submit</span>
                                        <span class="card-title white-text darken-1">You are Successfully Submit Your expense.</span>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span id="closeicon" aria-hidden="true">×</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->session->flashdata('field_missing')) : ?>
                                <div class="card-alert card red">
                                    <div class="card-content white-text">
                                        <span class="card-title white-text darken-1">
                                            <i class="material-icons">error_outline</i>Missing </span>
                                        <span class="card-title white-text darken-1">You Are Missing Some Important Felids. Please Resubmit Your Expense Thank You.</span>
                                    </div>
                                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span id="closeicon" aria-hidden="true">×</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                            <?php echo form_open('admin/addexpense'); ?>
                            <div class="row">
                                <div class="input-field col s8 m6 l4 right">
                                    <label for="date">Date</label>
                                    <input type="text" class="datepicker" name="expense_date" required>
                                </div>
                            </div>
                            <div class="input-field col s12">
                                <select name="expense_cat_id" class="select2 browser-default" required>
                                    <option disabled selected value="">Select Expense category </option>
                                    <?php foreach ($categories as $category) : ?>
                                        <option value="<?php echo $category['exp_cat_id']; ?>"><?php echo $category['exp_cat']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input-field col s12">
                                <input type="number" name="expense_price" placeholder="Add expense Price" required>
                            </div>
                            <div class="input-field col s12">
                                <input type="text" name="expense_note" placeholder="Add expense Note" required>
                            </div>
                            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>