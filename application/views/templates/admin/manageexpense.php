<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Expense </h5>
						</h5>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<section class="users-list-wrapper section">
							<div class="users-list-table">
								<!-- datatable start -->
								<div class="responsive-table">
									<table id="users-list-datatable" class="table">
										<thead>
											<tr>
												<th></th>
												<th></th>
												<th>S/N</th>
												<th>Expense Category</th>
												<th>Expense Price</th>
												<th>Expense Note</th>
												<th>edit</th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php
											$s_n = '1';
											foreach ($expenses as $expense) : ?>
												<tr>
													<td></td>
													<td></td>
													<td><?php echo $s_n ?></td>
													<td><?php echo $expense['exp_cat']; ?></td>
													<td><?php echo $expense['expense_date']; ?></td>
													<td><?php echo $expense['expense_price']; ?></td>
													<td><?php echo $expense['expense_note']; ?></td>
													<td>
														<a onclick="loadexpenseinfo(this.id)" id="<?php echo $expense['expense_id']; ?>" type="submit" href="#modal3" name="action">
															<i class="material-icons left">edit</i>
														</a>
													</td>

													<td></td>
													<td></td>
												</tr>

											<?php $s_n++;
											endforeach; ?>
											</tfoot>
									</table>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="modal3" class="modal">
<div class="modal-content"style="padding: 0;">
	</div>
</div>
<script>
	function loadexpenseinfo(expenseid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_expense_adminmodal/" + expenseid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<script>
	function del(id) {

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			icon: 'warning',
			dangerMode: true,
			buttons: {
				cancel: 'No, Please!',
				delete: 'Yes, Delete It'
			}
		}).then(function(willDelete) {
			if (willDelete) {
				var html = $.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>admin/expense_delete/" + id,
					// data: info,
					async: false
				}).responseText;

				if (html == "success") {
					$("#delete").html("delete success.");
					return true;

				}
				swal("Poof! Your record has been deleted!", {
					icon: "success",
				});
				setTimeout(location.reload.bind(location), 1000);
			} else {
				swal("Your imaginary file is safe", {
					title: 'Cancelled',
					icon: "error",
				});
			}
		});


	}
</script>