<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-alert card " style="background: #262362;">
               <div class="card-content white-text">
                  <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Product</h5>
                  </h5>
               </div>
            </div>
            <div class="row">
               <div class="col s12">
                  <section class="users-list-wrapper section">
                     <div class="users-list-table">
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th>Product ID</th>
                                    <th>Products</th>
                                    <th>image</th>
                                    <th>category</th>
                                    <th>supplier Name</th>
                                    <th>Market Price</th>
                                    <th>Trade Price</th>
                                    <th>Pack Size</th>
                                    <th>General Name</th>
                                    <th>Discount Distributer on %</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($products as $product) : ?>
                                    <tr>
                                       <td><?php echo $product['product_id']; ?></td>
                                       <td><?php echo $product['product']; ?></td>
                                       <td><img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($product['pro_img'])) {
                                                                                       echo $product['pro_img'];
                                                                                    } else {
                                                                                       echo "no-image.png";
                                                                                    }  ?>" width="64px"></td>
                                       <td><?php echo $product['suppliers']; ?></td>
                                       <td><?php echo $product['category']; ?></td>
                                       <td><?php echo $product['market_price']; ?></td>
                                       <td><?php echo $product['trade_price']; ?></td>
                                       <td><?php echo $product['pack_size']; ?></td>
                                       <td><?php echo $product['general_name']; ?></td>
                                       <td><?php echo $product['discount']; ?></td>


                                       <td>
                                          <a onclick="loadproductinfo(this.id)" id="<?php echo $product['product_id']; ?>">
                                             <i class="material-icons left">edit</i>
                                          </a>
                                       </td>
                                       <td>

                                          <a id="<?php echo $product['product_id']; ?>" onclick="del(this.id)">
                                             <i class="material-icons left">delete_forever</i>

                                       </td>
                                       <td></td>
                                    </tr>
                                 <?php endforeach; ?>
                                 </tfoot>
                           </table>
                        </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div id="modal3" class="modal">
      <div class="modal-content"style="padding: 0;">
      </div>
   </div>
   <script>
      function loadproductinfo(productid) {
         // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_edit_product_adminmodal/" + productid,
            success: function(data) {
               $(".modal-content").html(data);
               $('#modal3').modal('open');
            }
         });
      }
   </script>
   <script>
      function del(id) {

         swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            icon: 'warning',
            dangerMode: true,
            buttons: {
               cancel: 'No, Please!',
               delete: 'Yes, Delete It'
            }
         }).then(function(willDelete) {
            if (willDelete) {
               var html = $.ajax({
                  type: "GET",
                  url: "<?php echo base_url(); ?>admin/deleteproduct/" + id,
                  // data: info,
                  async: false
               }).responseText;

               if (html == "success") {
                  $("#delete").html("delete success.");
                  return true;

               }
               swal("Poof! Your record has been deleted!", {
                  icon: "success",
               });
               setTimeout(location.reload.bind(location), 1000);
            } else {
               swal("Your imaginary file is safe", {
                  title: 'Cancelled',
                  icon: "error",
               });
            }
         });


      }
   </script>