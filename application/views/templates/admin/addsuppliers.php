<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Supplier</h5>
						</h5>
					</div>
				</div>
				<div class="card-content">
					<div id="my-page" class="row">
						<div class="col s12 m12 l6 4 card-panel border-radius-6 login-card ">
							<?php if ($this->session->flashdata('registered')) : ?>
								<div class="card-alert card green">
									<div class="card-content white-text">
										<span class="card-title white-text darken-1">
											<i class="material-icons">done</i> Supplier Registered</span>
										<span class="card-title white-text darken-1">New Supplier Has Been Successfully Registered .</span>
									</div>
									<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
										<span id="closeicon" aria-hidden="true">×</span>
									</button>
								</div>
							<?php endif; ?>
							<?php if ($this->session->flashdata('email_false')) : ?>
								<div class="card-alert card red">
									<div class="card-content white-text">
										<span class="card-title white-text darken-1">
											<i class="material-icons">error_outline</i> Already Registered </span>
										<span class="card-title white-text darken-1">Your Supplier is Already Registered With This email. Please Resubmit Your Form with another email Thank You.</span>
									</div>
									<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
										<span id="closeicon" aria-hidden="true">×</span>
									</button>
								</div>
							<?php endif; ?>
							<?php if ($this->session->flashdata('field_missing')) : ?>
								<div class="card-alert card red">
									<div class="card-content white-text">
										<span class="card-title white-text darken-1">
											<i class="material-icons">error_outline</i> Missing </span>
										<span class="card-title white-text darken-1">You Are Missing Some Important Felids. Please Resubmit Your Form Thank You.</span>
									</div>
									<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
										<span id="closeicon" aria-hidden="true">×</span>
									</button>
								</div>
							<?php endif; ?>
							<?php echo form_open_multipart('admin/addsuppliers') ?>
							<div class="row">
								<div class="col s12">
									<div class="row">
										<div class="input-field col s12">
											<input id="name2" type="text" name="suppliers" required>
											<label for="name2">Supplier Name</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="name3" type="email" name="email" required>
											<label for="name3">Email</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="name4" type="text" name="address" required>
											<label for="name4">Address</label>
										</div>
									</div>

									<div id="file-upload" class="section">
										<div class="row section">
											<div class="col s12 m12 l12">
												<h6 for="img2" style="color: #00A99D!important;">Supllier Image</h6>
												<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" type="submit" name="action">submit
												<i class="material-icons left">save</i>
											</button>
										</div>

									</div>
								</div>
							</div>
							<?php echo form_close() ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	setInterval(function() {
		document.getElementById("closeicon").click();
	}, 5000);
</script>