<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="card">
                        <div class="card-content">
                            <?php echo form_open('admin/addstock'); ?>
                            <div class="row">
                                <div class="col s10">
                                    <h4 class="card-title">Add Stock</h4>
                                </div>
                                <div class="col s2 right">
                                    <input type="text" class="datepicker" name="stock_date" placeholder="Type Date" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <select class="select2 browser-default" id="categoryid" name="stock_product_id">
                                        <option disabled selected>Select Product</option>
                                        <?php foreach ($products as $product) : ?>
                                            <option value="<?php echo $product['product_id']; ?>"><?php echo $product['product']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <a type="submit" onclick="getSearch()" ; name="searchBtn" class="btn waves-effect waves-light btn" style="height: 47px;">Search
                                    <i class="material-icons left">search</i>
                                </a>
                                <div class="input-field col s6">
                                    <input type="number" name="batch no" placeholder="Type Your batch no" required>

                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input type="number" name="purchaseorderid" placeholder="00" required>
                                </div>
                                <div class="input-field col s6">
                                    <input type="number" name="new_stock" placeholder="Type Your New stock" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input type="text" class="datepicker" name="manufacturing_date" placeholder="Type Manufacturing Date" required>
                                </div>
                                <div class="input-field col s6">
                                    <input type="text" class="datepicker" name="expiredate" placeholder="Type Expire Date" required>
                                </div>
                            </div>
                            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1" id="view" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    function getSearch() {
        var value = document.getElementById("searchuser").value;
        var catid = document.getElementById("categoryid").value;

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/get_search_product",
            data: ({
                search: value,
                CategoryID: catid
            }),
            success: function(data) {
                $("#ecommerce-products").html(data);
            }
        });
    }
</script>