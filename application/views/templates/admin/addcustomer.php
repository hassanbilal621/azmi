<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-alert card " style="background: #262362;">
               <div class="card-content white-text">
                  <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Customer</h5>
                  </h5>
               </div>
            </div>
            <div class="card-content">
               <div id="my-page" class="row">
                  <div class="col s12 m12 l6 4 card-panel border-radius-6 login-card ">
                     <?php if ($this->session->flashdata('registered')) : ?>
                        <div class="card-alert card green">
                           <div class="card-content white-text">
                              <span class="card-title white-text darken-1">
                                 <i class="material-icons">done</i> Customer Registered</span>
                              <span class="card-title white-text darken-1">New Customer Has Been Successfully Registered .</span>
                           </div>
                           <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span id="closeicon" aria-hidden="true">×</span>
                           </button>
                        </div>
                     <?php endif; ?>
                     <?php if ($this->session->flashdata('field_missing')) : ?>
                        <div class="card-alert card red">
                           <div class="card-content white-text">
                              <span class="card-title white-text darken-1">
                                 <i class="material-icons">error_outline</i> Missing </span>
                              <span class="card-title white-text darken-1">You Are Missing Some Important Feilds. Plaese Resubmit Your Form Thank You.</span>
                           </div>
                           <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span id="closeicon" aria-hidden="true">×</span>
                           </button>
                        </div>
                     <?php endif; ?>
                     <?php echo form_open('admin/addcustomer') ?>
                     <div class="row">
                        <div class="col s12">
                           <div class="input-field col s12">
                              <input id="customer2" type="text" name="customer_name">
                              <label for="customer2">Customer</label>
                           </div>

                           <div class="input-field col s12">
                              <input id="address2" type="text" name="billing_address">
                              <label for="address2">Billing Address</label>
                           </div>
                        </div>
                     </div>

                     <div class="row">
                        <div class="input-field col s12">
                           <button class="waves-effect waves-light btn submit right z-depth-2 mb-1 ml-1" type="submit" name="action">submit
                              <i class="material-icons right">send</i>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
               <?php echo form_close() ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>