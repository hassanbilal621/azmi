<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Add Categories</h4>
                  <div class="row">
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Categories</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($categories as $category) : ?>
                              <tr>
                                 <td><?php echo $category['cat_id']; ?></td>
                                 <td><?php echo $category['category']; ?></td>
                                 <td>
                                    <button class="btn waves-effect waves-light blue modal-trigger mb-2 mr-1" type="submit" href="#modal2" name="action">Edit
                                       <i class="material-icons left">edit</i>
                                    </button>
                                    <div id="modal2" class="modal">
                                       <div class="modal-content">
                                          <div class="row">
                                             <div class="col s12">
                                                <div class="card">
                                                   <?php echo form_open('admin/managecategory') ?>
                                                   <div class="col s12">
                                                      <!-- Form with placeholder -->
                                                      <h4 class="card-title">Edit Categories</h4>
                                                      <div class="row">
                                                         <div class="input-field col s12">
                                                            <input id="name2" type="text" value="<?php echo $category['category']; ?>" name="category">
                                                            <label for="name2">Add Categories</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                                                            <i class="material-icons right">send</i>
                                                         </button>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <?php echo form_close() ?>
                                          </div>
                                       </div>
                                    </div>
                  </div>
                  <button class="btn btn-warning-cancel waves-effect waves-light red" type="submit" name="action">Delete
                     <i class="material-icons left">delete_forever</i>
                  </button>
                  </td>
                  </tr>

               <?php endforeach; ?>
               </tfoot>
               </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->