<div id="main">
	<div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<h4 class="card-title">expense Report</h4>
						<div class="row">
							<?php echo form_open('admin/expensereport'); ?>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" class="datepicker" id="first_date" name="starttime" required>
									<label for="expense2">Start Time</label>
								</div>
								<div class="input-field col s6">
									<input type="text" class="datepicker" id="last_date" name="endtime" required>
									<label for="address2">End Time</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<a class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 " onclick="getSearch()" ; id="view" type="submit" name="action">submit
										<i class="material-icons right">send</i>
</a>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
						<div class="row" id="report">
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>

<script>
	function getSearch() {
		var first_date = document.getElementById("first_date").value;
		var last_date = document.getElementById("last_date").value;


		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/get_expense_report",
			data: ({
				first_date: first_date,
				last_date: last_date
			}),
			success: function(data) {
				$("#report").html(data);
			}
		});
	}
</script>