<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">

	<!---/////////////////////////////////////////-------------logo-----------///////////////////////////////////////// -->

	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a class="brand-logo darken-1" href="<?php echo base_url(); ?>admin/">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="azmi" style="height: 40px;margin: -8px 1px -17px -3px;">
				<span class="logo-text hide-on-med-and-down" style="font-size: 15px;">Azmi Enterprises</span>
			</a>
			<a class="navbar-toggler" href="#">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>

	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow"id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">

		<!---/////////////////////////////////////////-------------Dashboard-----------///////////////////////////////////////// -->


		<li class="bold<?php if($this->router->fetch_method() == 'index') { echo 'active'; }?>">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/dashboard.png" alt="" style="height: 30px;margin: 0px 35px -11px 0;"><span class="menu-title color-02" data-i18n="">Dashboard</span></a>
		</li>

		<!---/////////////////////////////////////////-------------Suppliers-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'supplier' || $this->router->fetch_method() == 'managesuppliers'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/supplier.png" alt="" style="height: 40px;margin: 0 20px -13px -5px;">
				<span class="menu-title color-02" data-i18n="">Suppliers</span>
			</a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'supplier'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/supplier" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Suppliers</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'managesuppliers'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managesuppliers" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Suppliers</span></a></li>


				</ul>
			</div>
		</li>
		<!---/////////////////////////////////////////-------------expense Management-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'expensecategory' || $this->router->fetch_method() == 'expense' || $this->router->fetch_method() == 'manageexpense'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/expenses.png" alt="" style="height: 30px;margin: 0px 30px -7px 0px;"><span class="menu-title color-02" data-i18n="">expense</span></a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">

					<li class="<?php if($this->router->fetch_method() == 'expensecategory'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/expensecategory" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add expense Category</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'expense'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/expense" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add expense</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'manageexpense'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageexpense" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage expense</span></a></li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Product-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'category' || $this->router->fetch_method() == 'product' || $this->router->fetch_method() == 'manageproduct'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/product.png" alt="" style="height: 31px;margin: 0 30px -7px 0;"><span class="menu-title color-02" data-i18n="">Product</span></a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'category'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/category" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Category</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'product'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/product" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Product</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'manageproduct'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageproduct" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Product</span></a></li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Purchase Order-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'purchaseorder' || $this->router->fetch_method() == 'managepurchaseorder'||$this->router->fetch_method() == 'recivedpurchaseorder' || $this->router->fetch_method() == 'cancelpurchaseorder' || $this->router->fetch_method() == 'pandingpurchaseorder'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/order.png" alt="" style="height: 32px;margin: 0px 29px -7px 7px;"><span class="menu-title color-02" data-i18n="">Purchase Order</span></a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'purchaseorder'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/purchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Make Purchase Order</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'managepurchaseorder'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managepurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Purchase Order</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'recivedpurchaseorder'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/recivedpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Recived Purchase Order</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'pandingpurchaseorder'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/pandingpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Pending Purchase Order</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'cancelpurchaseorder'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/cancelpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Cancel Purchase Order</span></a></li>

				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Company-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'managecompany' || $this->router->fetch_method() == 'company'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/company.png" alt="" style="height: 32px;margin: 0px 27px -7px 0px;"><span class="menu-title color-02" data-i18n="">Company</span></a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'managecompany'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managecompany" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Company</span></a></li>
					<?php foreach ($companies as $company) : ?>
						<li class="<?php if($this->router->fetch_method() == 'company'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/company/<?php echo $company['company_id']; ?>" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span><?php echo $company['companyname']; ?></span></a></li>
					<?php endforeach; ?>

				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Customer-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'customer' || $this->router->fetch_method() == 'managecustomer'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/customer.png" alt="" style="height: 25px;margin: 0 25px -7px 0px;"><span class="menu-title color-02" data-i18n="">Customer</span></a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'customer'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/customer" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Customer</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'managecustomer'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managecustomer" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Company</span></a></li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Invoice-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'invoice' || $this->router->fetch_method() == 'manageinvoice'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/invoice.png" alt="" style="height: 31px;margin: 0px 26px -7px 7px;"><span class="menu-title color-02" data-i18n="">Invoice</span></a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'invoice'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/invoice" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Invoice</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'manageinvoice'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageinvoice" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Invoice</span></a></li>
				</ul>
			</div>
		</li>
		<!---/////////////////////////////////////////-------------stock-----------///////////////////////////////////////// -->

		<li class="bold <?php if($this->router->fetch_method() == 'managestock' || $this->router->fetch_method() == 'addstock'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/stock.png" alt="" style="height: 30px;margin: 0px 28px -7px 0px;"><span class="menu-title color-02" data-i18n="">stock</span></a>
			<div class="collapsible-body">
				<ul>
					<li class="<?php if($this->router->fetch_method() == 'addstock'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addstock" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Add Stock</span></a>
					</li>
					<li class="<?php if($this->router->fetch_method() == 'managestock'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managestock" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Manage Stock</span></a>
					</li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Payment-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'managepayment'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/payment.png" alt="" style="height: 30px;margin: 0px 22px -7px 0px;"><span class="menu-title color-02" data-i18n="">Payment</span></a>
			<div class="collapsible-body">
				<ul>
				
					<li class="<?php if($this->router->fetch_method() == 'managepayment'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managepayment" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Manage Payment</span></a>
					</li>
				</ul>
			</div>
		</li>
		<!---/////////////////////////////////////////-------------Report-----------///////////////////////////////////////// -->


		<li class="bold <?php if($this->router->fetch_method() == 'salereport' || $this->router->fetch_method() == 'purchasereport' ||$this->router->fetch_method() == 'expensereport' || $this->router->fetch_method() == 'salereport'){ echo 'active'; }?>">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/invoice.png" alt="" style="height: 31px;margin: 0px 33px -7px 0px;"><span class="menu-title color-02" data-i18n="">Reports</span></a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">
					<li class="<?php if($this->router->fetch_method() == 'salereport'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/salereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Sale Report</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'purchasereport'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/purchasereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Purchase Report</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'expensereport'){ echo 'active'; }?>"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/expensereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>expense Report</span></a></li>
					<li class="<?php if($this->router->fetch_method() == 'salereport'){ echo 'active'; }?>"><a  class="collapsible-body" href="<?php echo base_url(); ?>admin/salereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Unpaid Report</span></a></li>
				</ul>
			</div>
		</li>

		<li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/logout"">
				<img src=" <?php echo base_url(); ?>/assets/app-assets/images/icon/logout.png" alt="" style="height: 31px;margin: 0px 28px -7px 0px;"><span class="menu-title color-02" data-i18n="Chat">Logout</span></a>
		</li>

	</ul>
	<div class="navigation-background"></div>
	<a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->