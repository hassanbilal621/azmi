<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Stock</h5>
						</h5>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<section class="users-list-wrapper section">
							<div class="users-list-table">
								<div class="responsive-table">
									<table id="users-list-datatable" class="table">
										<thead>
											<tr>
												<!-- <th></th>
												<th></th> -->
												<th>Stock Id</th>
												<th>Stock Date</th>
												<th>Product Id</th>
												<th>Purchase Order Id</th>
												<th>Stock</th>
												<!-- <th>Used Stock</th> -->
												<th>Batch No</th>
												<th>Manufacturing Date</th>
												<th>Expire Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($stocks as $stock) : ?>
												<tr>
													<td><?php echo $stock['stock_id']; ?></td>
													<td><?php echo $stock['stock_date']; ?></td>
													<td><?php echo $stock['product']; ?></td>
													<td><?php echo $stock['purchaseorderid']; ?></td>
													<td><?php echo $stock['new_stock']; ?></td>
													<!--	<td><?php echo $stock['used_stock']; ?></td>-->
													<td><?php echo $stock['batch_no']; ?></td>
													<td><?php echo $stock['manufacturing_date']; ?></td>
													<td><?php echo $stock['expiredate']; ?></td>
													<td>
														<a onclick="loadstockinfo(this.id)" id="<?php echo $stock['stock_id']; ?>" type="submit" href="#modal3" name="action">
															<i class="material-icons left">edit</i>
														</a>
														<a id="<?php echo $stock['stock_id']; ?>" onclick="del(this.id)" type="submit" name="action">
															<i class="material-icons left">delete_forever</i>
														</a>
													</td>
												</tr>
											<?php endforeach; ?>
											</tfoot>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="modal3" class="modal">
	<div class="modal-content"style="padding: 0;">
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
	function loadstockinfo(stockid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_stock/" + stockid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<script>
	function del(id) {

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			icon: 'warning',
			dangerMode: true,
			buttons: {
				cancel: 'No, Please!',
				delete: 'Yes, Delete It'
			}
		}).then(function(willDelete) {
			if (willDelete) {
				var html = $.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>admin/stockdelete/" + id,
					// data: info,
					async: false
				}).responseText;

				if (html == "success") {
					$("#delete").html("delete success.");
					return true;

				}
				swal("Poof! Your record has been deleted!", {
					icon: "success",
				});
				setTimeout(location.reload.bind(location), 1000);
			} else {
				swal("Your imaginary file is safe", {
					title: 'Cancelled',
					icon: "error",
				});
			}
		});


	}
</script>