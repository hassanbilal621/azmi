<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3"><?php echo $company['companyname']; ?></h5>
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <section class="users-list-wrapper section">
                            <div class="users-list-table">
                                <!-- datatable start -->
                                <div class="responsive-table">
                                    <table id="users-list-datatable" class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Invoice id</th>
                                                <th>Date</th>
                                                <th>Customer Name</th>
                                                <th>Order By</th>
                                                <th>Order Note</th>
                                                <th>Grand Total</th>
                                                <th>View</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($invoices)) {
                                            ?>
                                                <?php foreach ($invoices as $invoice) : ?>
                                                    <tr>
                                                        <td></td>
                                                        <td><?php echo $invoice['invoice_id']; ?></td>
                                                        <td><?php echo $invoice['invoicedate']; ?></td>
                                                        <td><?php echo $invoice['customer_id']; ?></td>
                                                        <td><?php echo $invoice['order_by']; ?></td>
                                                        <td><?php echo $invoice['companyid']; ?></td>
                                                        <td><?php echo $invoice['order_note']; ?></td>
                                                        <td><?php echo $invoice['grand_total']; ?></td>
                                                        <td>
                                                            <a class="waves-effect waves-light  btn  submit box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/viewinvoice/<?php echo $invoice['invoice_id']; ?>" type="submit" name="action">View Invoice
                                                                <i class="material-icons left">visibility</i>
                                                            </a>

                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php } ?>
                                            </tfoot>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
    function loadinvoice(invoiceid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_Invoice/" + invoiceid,
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal3').modal('open');
            }
        });
    }
</script>
<script>
    function del(id) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            icon: 'warning',
            dangerMode: true,
            buttons: {
                cancel: 'No, Please!',
                delete: 'Yes, Delete It'
            }
        }).then(function(willDelete) {
            if (willDelete) {
                var html = $.ajax({
                    type: "GET",
                    url: "<?php echo base_url(); ?>admin/deleteproduct/" + id,
                    // data: info,
                    async: false
                }).responseText;

                if (html == "success") {
                    $("#delete").html("delete success.");
                    return true;

                }
                swal("Poof! Your record has been deleted!", {
                    icon: "success",
                });
                setTimeout(location.reload.bind(location), 1000);
            } else {
                swal("Your imaginary file is safe", {
                    title: 'Cancelled',
                    icon: "error",
                });
            }
        });


    }
</script>
<script>
    $(document).ready(function() {
        $("#editcompanybtn").click(function() {
            $("#editcompanydiv").show();
            $("#editcompanybtn").hide();
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#savecompanybtn").click(function() {
            $("#editcompanydiv").hide();
            $("#editcompanybtn").show();
        });
    });
</script>