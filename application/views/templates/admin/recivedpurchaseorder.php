<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Received Purchase Order</h5>
						</h5>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<section class="users-list-wrapper section">
							<div class="users-list-table">
								<!-- datatable start -->
								<div class="responsive-table">
									<table id="users-list-datatable" class="table">
										<thead>
											<tr>
												<th></th>
												<th></th>
												<th></th>
												<th>id</th>
												<th>Date</th>
												<th>Supplier Name</th>
												<th>No_of_Product</th>
												<th>Grand Total</th>
												<th>Order Status</th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($purchaseorders as $purchaseorder) : ?>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td><?php echo $purchaseorder['purchase_order_id']; ?></td>
													<td><?php echo $purchaseorder['date']; ?></td>
													<td><?php echo $purchaseorder['suppliers']; ?></td>
													<?php
													$purchaseorderid = $purchaseorder['purchase_order_id'];
													$purchaseorderinvoice = $this->db->query("SELECT * FROM purchase_order_item WHERE purchase_order_id=$purchaseorderid");
													$purchaseorderinvoice1 = $purchaseorderinvoice->num_rows();
													?>
													<td><?php echo $purchaseorderinvoice1 ?></td>
													<td><?php echo $purchaseorder['grand_total']; ?></td>
													<?php if ($purchaseorder['order_status'] == 'cancel') { ?>

														<td class="chip red lighten-4">cancel</td>

													<?php } elseif ($purchaseorder['order_status'] == 'pending') { ?>

														<td class="chip lighten-3 orange">Pending</td>

													<?php } else { ?>
														<td class="chip green lighten-5">Recived</td>

													<?php } ?>

													<td></td>
													<td></td>
												</tr>
											<?php endforeach; ?>
											</tfoot>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>