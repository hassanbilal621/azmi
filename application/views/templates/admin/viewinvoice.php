<style>
   span {
      color: #272361 !important;
   }
</style>
<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="container">
            <!-- app invoice View Page -->
            <section class="invoice-view-wrapper section">
               <div class="row">
                  <!-- invoice view page -->
                  <div class="col xl9 m8 s12">
                     <div class="card">
                        <div class="card-content invoice-print-area">
                           <!-- header section -->
                           <div class="row invoice-date-number">
                              <div class="col xl4 s12">
                                 <span class="invoice-number mr-1">Invoice#</span>
                                 <span><?php echo $invoice['invoice_id']; ?></span>
                              </div>
                              <div class="col xl8 s12">
                                 <div class="invoice-date display-flex align-items-center flex-wrap">
                                    <div class="mr-3">
                                       <small>Date Issue:</small>
                                       <span><?php echo $invoice['invoicedate']; ?></span>
                                    </div>
                                    <!-- <div>
                                       <small>Date Due:</small>
                                       <span>08/10/2019</span>
                                    </div> -->
                                 </div>
                              </div>
                           </div>
                           <!-- logo and title -->
                           <div class="row mt-3 invoice-logo-title">
                              <div class="col m6 s12 display-flex invoice-logo mt-1 push-m6">
                                 <img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="logo" height="100" width="164">
                              </div>
                              <div class="col m6 s12 pull-m6">
                                 <h4 class="indigo-text">Invoice</h4>
                                 <span>Azmi Enterprices</span>
                              </div>
                           </div>
                           <div class="divider mb-3 mt-3"></div>
                           <!-- invoice address and contact -->
                           <div class="row invoice-info">
                              <div class="col m6 s12">
                                 <h6 class="invoice-from">Bill From</h6>
                                 <div class="invoice-address">
                                    <span><?php echo $invoice['companyname']; ?></span>
                                 </div>
                                 <div class="invoice-address">
                                    <span><?php echo $invoice['companyaddress']; ?>.</span>
                                 </div>
                                 <!-- <div class="invoice-address">
                                    <span>hello@clevision.net</span>
                                 </div>
                                 <div class="invoice-address">
                                    <span>601-678-8022</span>
                                 </div> -->
                              </div>
                              </p>
                              <p></p>
                              <div class="col m6 s12">
                                 <div class="divider show-on-small hide-on-med-and-up mb-3"></div>
                                 <h6 class="invoice-to">Bill To</h6>
                                 <div class="invoice-address">
                                    <span><?php echo $invoice['customer_name']; ?>.</span>
                                 </div>
                                 <div class="invoice-address">
                                    <span><?php echo $invoice['billing_address']; ?></span>
                                 </div>
                                 <!-- <div class="invoice-address">
                                    <span>pixinvent@gmail.com</span>
                                 </div>
                                 <div class="invoice-address">
                                    <span>987-352-5603</span>
                                 </div> -->
                              </div>
                           </div>
                           <div class="divider mb-3 mt-3"></div>
                           <!-- product details table-->
                           <div class="invoice-product-details">
                              <table class="striped responsive-table">
                                 <thead>
                                    <tr>
                                    <tr>
                                       <th>S/N</th>
                                       <th>product Id</th>
                                       <th>Pack Size</th>
                                       <th>Qty</th>
                                       <th>Batch</th>
                                       <th>Expiry</th>
                                       <th>Price</th>
                                       <th>Discount</th>
                                       <th>Total Amount</th>
                                    </tr>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $s_n = 1; ?>
                                    <?php foreach ($invoiceitems as $invoiceitem) : ?>
                                       <tr>
                                          <td><?php echo $s_n; ?></td>
                                          <td><?php echo $invoiceitem['product_name_id']; ?></td>
                                          <td><?php echo $invoiceitem['pack_size']; ?></td>
                                          <td><?php echo $invoiceitem['Qty']; ?></td>


                                          <td><?php echo $invoiceitem['batch_no']; ?></td>
                                          <td><?php echo $invoiceitem['expiredate']; ?></td>

                                          <td><?php echo $invoiceitem['market_price']; ?></td>
                                          <td><?php echo $invoiceitem['discount']; ?>%</td>
                                          <td>RS:<?php echo $invoiceitem['netamount']; ?></td>
                                       </tr>
                                    <?php
                                       $s_n++;
                                    endforeach; ?>

                                 </tbody>
                              </table>
                           </div>
                           <!-- invoice subtotal -->
                           <div class="divider mt-3 mb-3"></div>
                           <div class="invoice-subtotal">
                              <div class="row">
                                 <div class="col m5 s12">
                                    <p>Thanks for your business.</p>
                                    <p><?php echo $invoice['order_note']; ?></p>
                                    <p class="strong">Order By:<?php echo $invoice['order_by']; ?></p>
                                 </div>
                                 <div class="col xl4 m7 s12 offset-xl3">
                                    <ul>
                                       <li class="display-flex justify-content-between">
                                          <span class="invoice-subtotal-title">Subtotal</span>
                                          <h6 class="invoice-subtotal-value">RS:<?php echo $invoice['grand_total']; ?></h6>
                                       </li>
                                       <li class="display-flex justify-content-between">
                                          <span class="invoice-subtotal-title">Discount</span>
                                          <h6 class="invoice-subtotal-value">0%</h6>
                                       </li>
                                       <li class="display-flex justify-content-between">
                                          <span class="invoice-subtotal-title">Tax</span>
                                          <h6 class="invoice-subtotal-value">0%</h6>
                                       </li>
                                       <li class="divider mt-2 mb-2"></li>
                                       <li class="display-flex justify-content-between">
                                          <span class="invoice-subtotal-title">Invoice Total</span>
                                          <h6 class="invoice-subtotal-value">RS:<?php echo $invoice['grand_total']; ?></h6>
                                       </li>
                                       <li class="display-flex justify-content-between">
                                          <span class="invoice-subtotal-title">Paid to date</span>
                                          <h6 class="invoice-subtotal-value">0</h6>
                                       </li>
                                       <li class="display-flex justify-content-between">
                                          <span class="invoice-subtotal-title">Balance (PKR)</span>
                                          <h6 class="invoice-subtotal-value">RS:<?php echo $invoice['grand_total']; ?></h6>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- invoice action  -->
                  <div class="col xl3 m4 s12">
                     <div class="card invoice-action-wrapper">
                        <div class="card-content">

                           <div class="invoice-action-btn">
                              <a href="#" class="btn-block btn btn-light-indigo waves-effect waves-light invoice-print">
                                 <span>Print</span>
                              </a>
                           </div>
                           <!-- <div class="invoice-action-btn">
                              <a href="app-invoice-edit.html" class="btn-block btn btn-light-indigo waves-effect waves-light">
                                 <span>Edit Invoice</span>
                              </a>
                           </div> -->
                           <!-- <div class="invoice-action-btn">
                              <a href="#" class="btn waves-effect waves-light display-flex align-items-center justify-content-center">
                                 <i class="material-icons mr-3">attach_money</i>
                                 <span class="text-nowrap">Add Payment</span>
                              </a>
                           </div> -->
                        </div>
                     </div>
                  </div>
               </div>
            </section>

         </div>
      </div>
   </div>
  