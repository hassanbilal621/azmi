<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Invoice</h5>
                        </h5>
                    </div>
                </div>
                <div class="card-content">
                    <?php if ($this->session->flashdata('Submit_invoice')) : ?>
                        <div class="card-alert card green">
                            <div class="card-content white-text">
                                <span class="card-title white-text darken-1">
                                    <i class="material-icons">done</i> Invoice Submit</span>
                                <span class="card-title white-text darken-1">New Supplier Has Been Successfully Submit your Invoice .</span>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span id="closeicon" aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->session->flashdata('field_missing')) : ?>
                        <div class="card-alert card red">
                            <div class="card-content white-text">
                                <span class="card-title white-text darken-1">
                                    <i class="material-icons">error_outline</i> Missing </span>
                                <span class="card-title white-text darken-1">You Are Missing Some Important Feilds. Plaese Resubmit Your Invoice Thank You.</span>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span id="closeicon" aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>
                    <?php echo form_open('admin/addinvoice'); ?>
                    <div class="row">
                        <div class="col s2 right">
                            <label>Invoice Date</label>
                            <input type="text" class="datepicker" name="invoicedate" required>
                        </div>
                    </div>
                    <div class="box box-block bg-white">
                        <div class="row">
                            <div class="input-field col s6">
                                <select class="select2 browser-default" name="customer_id">
                                    <option value="" disabled selected>Select Customer</option>
                                    <?php foreach ($customers as $customer) : ?>
                                        <option value="<?php echo $customer['customer_id']; ?>"><?php echo $customer['customer_name']; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="input-field col s6">
                                <input name="order_by" type="text" required>
                                <label>Order by</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input name="order_note" type="text" required>
                                <label>Order Note For Customer</label>
                            </div>
                            <div class="input-field col s6">
                                <select class="select2 browser-default" name="companyid">
                                    <option value="" disabled selected>Select Company</option>
                                    <?php foreach ($companies as $company) : ?>
                                        <option value="<?php echo $company['company_id']; ?>"><?php echo $company['companyname']; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box box-block bg-white">
                        <div class="row">
                            <div class="col s3">
                                <select class="select2 browser-default" name="product_id" class="select2" id="product_id" onchange="changeProductvalue(this.value)">
                                    <option value="-23" disabled selected>Select Product</option>
                                    <?php foreach ($products as $product) : ?>
                                        <option value="<?php echo $product['product_id']; ?>">
                                            <?php echo $product['product']; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col s3">
                                <select class="select2 browser-default" name="stockid" class="select2" id="stocks_load" onchange="changestockvalue(this.value)" class="browser-default">
                                    <option value="" disabled selected>Select stock</option>
                                    <?php foreach ($stocks as $stock) : ?>
                                        <option value="<?php echo $stock['stock_id']; ?>"><?php echo $stock['stock_id']; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="input-field col s2">
                                <input type="number" name='batch_no' value="" id="batch_no" placeholder='0.00' class="form-control batch_no" readonly />
                                <label for="batch_no">Batch No</label>
                            </div>
                            <div class="input-field col s2">
                                <input type="text" name='expiredate' value="" id="expiredate" placeholder='0.00' class="form-control price" step="0.00" min="0" readonly />
                                <label for="expiredate">Expire Date</label>
                            </div>
                            <div class="input-field col s2">
                                <input type="number" name='final_stock' id="final_stock" value="" placeholder='0.00' class="form-control total" readonly />
                                <label for="final_stock">Final Stock</label>
                            </div>
                        </div>
                        <div class=" row">
                            <div class="input-field col s2">
                                <input type="number" name='market_price' value="" id="marketprice" placeholder='0.00' class="form-control price" step="0.00" min="0" readonly />
                                <label for="market_price">Price</label>
                            </div>
                            <div class="input-field col s2">
                                <input onkeyup="onpay(this.value)" name='qty' id="qty" max="0" placeholder='0.00' class="qty validate" step="0" min="1" type="number" pattern="[0-9]*" />
                                <label for="qty">Qty</label>
                            </div>
                            <div class="input-field col s2">
                                <input type="number" name='amount' id="amount" value="" placeholder='0.00' class="form-control total" readonly />
                                <label for="amount">Amount</label>
                            </div>
                            <div class="input-field col s2">
                                <input type="number" name='discount' onkeyup="calculatediscount(this.value)" id="discount" value="" placeholder='0.00' class="form-control total" />
                                <label for="discount">Discount</label>
                            </div>
                            <div class="input-field col s2">
                                <input type="number" name='totalamount' id="totalamount" value="" placeholder='0.00' class="form-control total" readonly />
                                <label for="totalamount">Total Amount</label>
                            </div>
                            <div class="center col input-field s2">
                                <a id="add_row"><i class="material-icons">add</i></a>
                            </div>
                            <table class="table table-bordered table-hover" id="tab_logic">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Discount</th>
                                        <th>Stock ID</th>
                                        <th>Net Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="dynamic_field">
                                </tbody>
                            </table>
                            <div class="row clearfix" style="margin-top:20px">
                                <div class="col-md-12 right">
                                    <table class="table table-bordered table-hover" id="tab_logic_total">
                                        <tbody>
                                            <tr>
                                                <th class="text-center">Grand Total</th>
                                                <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row clearfix" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <button type="submit" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right">Submit
                                        Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script>
    function calculatediscount(discountamount) {
        var finalamount = Number(discountamount) / 100;
        var getamount = Number(document.getElementById("amount").value) * finalamount;
        var calculatedamount = Number(document.getElementById("amount").value) - getamount;
        result = Math.round(calculatedamount * 100) / 100
		document.getElementById("totalamount").value = result;


    }
</script>
<script>
    function trprice(trprice) {
        document.getElementById("qty").value = 1;
        document.getElementById("amount").value = trprice;

    }

    function onpay(qty) {
        var payamount = document.getElementById("marketprice").value;
        var total = Number(qty) * Number(payamount);
        document.getElementById("amount").value = total;
        discountamount = document.getElementById("discount").value;
        var finalamount = Number(discountamount) / 100;
        var getamount = Number(document.getElementById("amount").value) * finalamount;
        var calculatedamount = Number(document.getElementById("amount").value) - getamount;
        document.getElementById("totalamount").value = calculatedamount;


    }
</script>
<script>
    function changeProductvalue(productid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_get_product_details/" + productid,
            success: function(data) {
                var obj = JSON.parse(data);
                document.getElementById("marketprice").value = obj.market_price;
                document.getElementById("qty").value = 1;
                document.getElementById("amount").value = obj.market_price;
                document.getElementById("discount").value = obj.discount;
                var finalamount = Number(obj.discount) / 100;
                var getamount = Number(obj.market_price) * finalamount;
                var calculatedamount = Number(obj.market_price) - getamount;
                document.getElementById("totalamount").value = calculatedamount;
            }
        });
        //alert('asdasdasd');
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/get_stock_by_poductid/" + productid,
            data: 'country_name=' + productid,
            success: function(data) {
                $("#stocks_load").html(data);
                //alert('fucntiondoen');
            }
        });

        // alert('fucntiondoen1');
    }
</script>

<script>
    function changestockvalue(stockid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_get_stock_details/" + stockid,
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(obj);
                // document.getElementById("new_stock").value = Number(obj.new_stock);
                // document.getElementById("used_stock").value = Number(obj.used_stock);
                var myEle = document.getElementById("sto"+stockid);
                if(myEle){
                    alert("Stock id already exisit");
                }else{
                    document.getElementById("batch_no").value = obj.batch_no;
                    document.getElementById("expiredate").value = obj.expiredate;
                    var new_stock = Number(obj.new_stock);
                    var final_stock = new_stock - Number(obj.used_stock);
                    document.getElementById("final_stock").value = final_stock;
                    document.getElementById("qty").max = final_stock;
                }

            }
        });
    }
</script>

<script>
    $(document).ready(function() {
        var finaltotal = 0;
        var i = 1;
        $('#add_row').click(function() {
            var e = document.getElementById("product_id");
            var productname = e.options[e.selectedIndex].text;
            var productid = document.getElementById("product_id").value;
            var marketprice = document.getElementById("marketprice").value;
            var qty = document.getElementById("qty").value;
            var total = document.getElementById("amount").value;
            var discount = document.getElementById("discount").value;
            var stockid = document.getElementById("stocks_load").value;
            var totalamount = document.getElementById("totalamount").value;
            alert(document.getElementById("qty").max);

            if (document.getElementById("stocks_load").value != "") {
                if (Number(document.getElementById("qty").max) >= Number(document.getElementById("qty").value)) {
                    i++;



                    $('#dynamic_field').append('<tr id="row' + i + '"><td>' + productname +
                        '<input type="hidden" name="products[]" value="' + productid +
                        '" id="Product" placeholder="Product Name" class="form-control Product" /></td><td><input type="number" name="marketprice[]" value="' +
                        marketprice +
                        '" readonly placeholder="Trade Price" class="form-control price" step="0.00" min="0" /></td><td><input type="number" name="qty[]" placeholder="Enter Qty" value="' +
                        qty +
                        '" class="form-control qty" readonly step="0" min="0" /></td><td><input type="number" name="discount[]" readonly placeholder="0.00" class="form-control total" value="' +
                        discount + '" readonly /></td><td><input type="number" id="sto'+stockid +'" name="stockid[]" readonly placeholder="0.00" class="form-control total" value="' +
                        stockid + '" readonly /></td><td><input type="number" name="total[]" id="totalamount' +
                        i + '" placeholder="0.00" class="form-control total" value="' + totalamount +
                        '" readonly /></td><td><a name="remove" id="' + i +
                        '" class=" right btn_remove"><i class="material-icons">delete</i></a> </td></tr>'
                    );

                    finaltotal = Number(finaltotal) + Number(totalamount);
                    result = Math.round(finaltotal * 100) / 100
                    document.getElementById("total_amount").value = result;
                    $('#product_id').val('-23'); // Select the option with a value of '1'
                    $('#product_id').trigger('change'); // Notify any JS components that the value changed


                    // document.getElementById("product_id").value = 0;
                    // $("#product_id").select2({
                    //     placeholder: "Select a state"
                    // });
                    //$("#product_id option:selected").prop('selected' , false)
                    // $("#product_id").empty();
                }
            }
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            getrowvalue = document.getElementById("totalamount" + button_id).value;
            alert(getrowvalue);
            finaltotal = finaltotal - Number(getrowvalue);
            result = Math.round(finaltotal * 100) / 100
                    document.getElementById("total_amount").value = result;
            $('#row' + button_id + '').remove();
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>