
<div class="row login-bg">
	<div class="col s12">
		<div class="container">
			<div id="login-page" class="row">
				<div class="col s12 m12 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
					<?php echo form_open('admin/login'); ?>
					<div class="login-form">
						<div class="row">
							<div class="input-field col s12 center">
								<img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="azmi" style="margin: auto;width: 100px;">

								<h6 class="center login-form-text">Welcome to the Azmi Enterprises Admin</h6>
							</div>
						</div>
						<?php if ($this->session->flashdata('login_failed')) : ?>
							<div id="card-alert" id="car" class="card red">
								<div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
									<p> <?php echo $this->session->flashdata('login_failed'); ?></p>
								</div>

							</div>
						<?php endif; ?>
						<?php if ($this->session->flashdata('password_changed')) : ?>
							<div id="card-alert" id="car" class="card green">
								<div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
									<p> <?php echo $this->session->flashdata('password_changed'); ?></p>
								</div>

							</div>
						<?php endif; ?>
						<?php if ($this->session->flashdata('user_registered')) : ?>
							<div id="card-alert" class="card green">
								<div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
									<p> <?php echo $this->session->flashdata('user_registered'); ?></p>
								</div>

							</div>
						<?php endif; ?>
						<?php if ($this->session->flashdata('user_loggedout')) : ?>
							<div id="card-alert" class="card red">
								<div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
									<p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
								</div>

							</div>
						<?php endif; ?>
						<div class="row margin">
							<div class="input-field col s12">
								<i style="color:#26a1f5;" class="material-icons prefix pt-2">person_outline</i>
								<input id="username" name="email" type="email" placeholder="Type Your Email" required>
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12">
								<i style="color:#26a1f5;" class="material-icons prefix pt-2">lock_outline</i>
								<input id="password" name="password" type="password" placeholder="Password" required>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<button type="submit" name="login" class="btn waves-effect submit border-round waves-light col s12">Login</button>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m6 l6">
								<!-- <p class=" medium-small"><a style="font-size: large;" href="<?php echo base_url(); ?>admin/register">Register Now!</a></p> -->
							</div>
							<div class="col s12 m6 l6">
								<p class="right medium-small"><a style="font-size: large;" href="<?php echo base_url(); ?>admin/forgetpassword">Forget Password!</a></p>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>