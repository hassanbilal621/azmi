<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
  <div class="footer-copyright">
    <div class="container"><span>&copy; 2020 | All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a>Softologics.com</a></span></div>
  </div>
</footer>

<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/data-tables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/extra-components-sweetalert.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-file-uploads.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/formatter/jquery.formatter.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/dropify/js/dropify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/datatables.checkboxes.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/datatables.checkboxes.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/charts-sparklines.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/select2/select2.full.min.js"></script>

<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/ui-alerts.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/page-users.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/app-invoice.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-select2.js"></script>

</body>

</html>
