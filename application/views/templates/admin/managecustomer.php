<div id="main">
	<div class="row">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-alert card " style="background: #262362;">
						<div class="card-content white-text">
							<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Customer</h5>
							</h5>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<section class="users-list-wrapper section">
								<div class="users-list-table">
									<div class="responsive-table">
										<table id="users-list-datatable" class="table">
											<thead>
												<tr>
													<th></th>
													<th></th>
													<th></th>
													<th>Customer ID</th>
													<th>Customer</th>
													<th>Billing Address</th>
													<th>Account Balance</th>
													<th>Status</th>
													<th> </th>
													<th> </th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($customers as $customer) : ?>
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td><?php echo $customer['customer_id']; ?></td>
														<td><?php echo $customer['customer_name']; ?></td>
														<td><?php echo $customer['billing_address']; ?></td>

														<?php
														$customerid = $customer['customer_id'];
														$invoicesdata = $this->db->query("SELECT * FROM invoice WHERE invoice.customer_id = $customerid");

														$customerinvoices = $invoicesdata->result_array();
														$totalamount = 0;
														foreach ($customerinvoices as $invoice) :
															$totalamount = $totalamount + $invoice['grand_total'];
														endforeach; ?>

														<td>PKR <?php echo $totalamount; ?></td>


														<td>
															<a onclick="loadcustomerinfo(this.id)" id="<?php echo $customer['customer_id']; ?>">
																<i class="material-icons left">edit</i>
															</a>
														</td>
														<!-- <td>
															<a id="<?php echo $customer['customer_id']; ?>" onclick="del(this.id)">
																<i class="material-icons left">delete_forever</i>
															</a>
														</td> -->
														<td></td>
														<td></td>
													</tr>
												<?php endforeach; ?>
												</tfoot>
										</table>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div id="modal3" class="modal">
	<div class="modal-content" style="padding: 0;">
	</div>
</div>
<script>
	function loadcustomerinfo(customerid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_customer_adminmodal/" + customerid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<script>
	function del(id) {

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			icon: 'warning',
			dangerMode: true,
			buttons: {
				cancel: 'No, Please!',
				delete: 'Yes, Delete It'
			}
		}).then(function(willDelete) {
			if (willDelete) {
				var html = $.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>admin/customerdelete/" + id,
					// data: info,
					async: false
				}).responseText;

				if (html == "success") {
					$("#delete").html("delete success.");
					return true;

				}
				swal("Poof! Your record has been deleted!", {
					icon: "success",
				});
				setTimeout(location.reload.bind(location), 1000);
			} else {
				swal("Your imaginary file is safe", {
					title: 'Cancelled',
					icon: "error",
				});
			}
		});


	}
</script>