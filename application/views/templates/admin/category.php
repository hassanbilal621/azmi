<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-alert card " style="background: #262362;">
                    <div class="card-content white-text">
                        <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Add Categories</h5>
                        </h5>
                    </div>
                </div>
                <?php if ($this->session->flashdata('_added')) : ?>
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <span class="card-title white-text darken-1">
                                <i class="material-icons">done</i>Category register</span>
                            <span class="card-title white-text darken-1">You are Successfully register category .</span>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span id="closeicon" aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('_delete')) : ?>
                    <div class="card-alert card red">
                        <div class="card-content white-text">
                            <span class="card-title white-text darken-1">
                                <i class="material-icons">error_outline</i>Category Delete</span>
                            <span class="card-title white-text darken-1">You are Successfully Delete category.</span>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span id="closeicon" aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('field_missing')) : ?>
                    <div class="card-alert card red">
                        <div class="card-content white-text">
                            <span class="card-title white-text darken-1">
                                <i class="material-icons">error_outline</i>Missing </span>
                            <span class="card-title white-text darken-1">You Are Missing Some Important Feilds. Plaese Resubmit Your Form Thank You.</span>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span id="closeicon" aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('already')) : ?>
                    <div class="card-alert card red">
                        <div class="card-content white-text">
                            <span class="card-title white-text darken-1">
                                <i class="material-icons">error_outline</i>Already Category registered</span>
                            <span class="card-title white-text darken-1">You Are Already register Category. Plaese Resubmit Your Form Thank You.</span>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span id="closeicon" aria-hidden="true">×</span>
                        </button>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col s12">
                        <?php echo form_open('admin/addcategory'); ?>
                        <div class="input-field col s12">
                            <input type="text" name="category" placeholder="Add category" required>
                        </div>
                        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div id="highlight-table" class="card card card-default scrollspy">
                            <div class="card-content">
                                <table class="highlight striped">

                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>category Name</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php   $s_n = '1';
                                        foreach ($categories as $category) : ?>
                                            <tr>
                                                <td><?php echo $s_n ?></td>
                                                <td><?php echo $category['category']; ?></td>
                                                <td>
                                                    <a onclick="loadcategoryinfo(this.id)" id="<?php echo $category['category_id']; ?>">
                                                        <i class="material-icons left">edit</i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php   $s_n ++ ;
                                     endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>



<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
<script>
    function loadcategoryinfo(categoryid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_edit_category/" + categoryid,
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal3').modal('open');
            }
        });
    }
</script>
<script>
    setInterval(function() {
        document.getElementById("closeicon").click();
    }, 5000);
</script>
<script>
    function deactive(categoryid) {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/deactivecategory/" + categoryid,
            success: function(data) {
                swal({
                    title: 'Deactive category',
                    icon: 'error',
                    timer: 1000,
                }); {
                    setTimeout(location.reload.bind(location), 1500);
                }
            }
        });
    }
</script>

<script>
    function active(categoryid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/activecategory/" + categoryid,
            success: function(data) {
                swal({
                    title: 'Active category',
                    icon: 'success',
                    timer: 1000,
                }); {
                    setTimeout(location.reload.bind(location), 1500);
                }
            }
        });
    }
</script>