<div class="row" id="stock_products" style="padding: 0px 10px;">
    <?php form_open('admin/addstock') ?>
    <input value="<?php echo $purchaseorderid; ?>" type="hidden" name="purchaseorderid">

    <div class="row">
        <div class="col s12">
            <section class="users-list-wrapper section">
                <div class="users-list-table">
                    <div class="responsive-table">
                        <table id="users-list-datatable" class="table">
                            <thead>
                                <tr>
                                    <th>Product id</th>
                                    <th>Product</th>
                                    <th>Batch No</th>
                                    <th>Manufacturing Date</th>
                                    <th>Expire Date</th>
                                    <th>Purchase Order Qty</th>
                                    <th>Remaing Stock</th>
                                    <th>Type New Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($purchaseorderitems as $purchaseorderitem) : ?>
                                    <?php
                                    $product_id = $purchaseorderitem['product_name_id'];
                                    $poid = $purchaseorders['purchase_order_id'];

                                    $this->db->where('stock_product_id', $product_id);
                                    $this->db->where('purchaseorderid', $poid);
                                    $query = $this->db->get('stock');
                                    $stockdataresults =  $query->result_array();



                                    $totalstockinpo = $purchaseorderitem['Qty'];


                                    $totalupdatestock = 0;
                                    foreach($stockdataresults as $stockdataresult):
                                        if (isset($stockdataresult['new_stock'])) {
                                            $totalupdatestock = $totalupdatestock + intval($stockdataresult['new_stock']);
                                        }
                                    
                                    endforeach;


                                    $remainingamount = $totalstockinpo - $totalupdatestock;
                                    if ($remainingamount <= '0') {
                                        $need = 'readonly';
                                    } else {
                                        $need = '';
                                    }
                                    
                                    ?>
                                    <tr>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="product" value="<?php echo $purchaseorderitem['product_name_id']; ?>" type="number" name="product_id[]" <?php echo " $need" ?> style="border: unset;color: black!important;text-align: center;font-size: inherit;">

                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <h6><?php echo $purchaseorderitem['product']; ?></h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="batch_no" type="number" name="batch_no[]" placeholder="Batch No" <?php echo "$need" ?>>
                                                <input type="hidden" name="product[]" value="<?php echo $purchaseorderitem['product']; ?>">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="Manufacturing_date" type="text" class="datepicker" name="manufacturing_date[]" placeholder="Manufacturing Date" <?php echo "$need" ?>>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <input id="packsize" type="text" class="datepicker" name="expiredate[]" placeholder="Expire Date" <?php echo "$need" ?>>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">
                                                <h6> <?php echo $purchaseorderitem['Qty']; ?></h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">

                                                <h6> <?php echo $remainingamount ?></h6>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-field col s12">

                                                <input type="hidden" name="stock_po_id[]" value="<?php echo $purchaseorders['purchase_order_id']; ?>">
                                                <input id="type_new_stock" type="number" name="new_stock[]" placeholder="Type New Stock" min="1" max="<?php echo $remainingamount ?>" <?php echo "$need" ?>>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                endforeach ?>
                            </tbody>
                        </table>
                        <button class="waves-effect waves-light btn submit z-depth-2 right mt-1 ml-1" id="view" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                        <?php form_close() ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>