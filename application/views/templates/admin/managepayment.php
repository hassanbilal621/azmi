<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Payment </h5>
						</h5>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<section class="users-list-wrapper section">
							<div class="users-list-table">
								<div class="responsive-table">
									<table id="users-list-datatable" class="table">
										<thead>
											<tr>
												<th></th>
												<th></th>
												<th>payment Id</th>
												<th>Payment Date</th>
												<th>Payment</th>
												<th>Payment Note</th>
												<th>Purchase Order Id</th>
												<th>edit</th>
												<th>delete</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($payments as $payment) : ?>
												<tr>
													<td></td>
													<td></td>
													<td><?php echo $payment['payment_id']; ?></td>
													<td><?php echo $payment['paymentdate']; ?></td>
													<td><?php echo $payment['payment']; ?></td>
													<td><?php echo $payment['paymentnote']; ?></td>
													<td><?php echo $payment['purchass_order_id_payment']; ?></td>
													<td>
														<a onclick="loadpaymentinfo(this.id)" id="<?php echo $payment['payment_id']; ?>" type="submit" href="#modal3" name="action">
															<i class="material-icons left">edit</i>
														</a>
													</td>
													<td>
														<a id="<?php echo $payment['payment_id']; ?>/<?php echo $payment['purchass_order_id_payment']; ?> " onclick="del(this.id)" type="submit" name="action">
															<i class="material-icons left">delete_forever</i>
														</a>
													</td>
													<td></td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>



<div id="modal3" class="modal">
	<div class="modal-content">
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
	function loadpaymentinfo(paymentid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_payment/" + paymentid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<script>
	function del(id) {

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			icon: 'warning',
			dangerMode: true,
			buttons: {
				cancel: 'No, Please!',
				delete: 'Yes, Delete It'
			}
		}).then(function(willDelete) {
			if (willDelete) {
				var html = $.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>admin/paymentdelete/" + id,
					// data: info,
					async: false
				}).responseText;

				if (html == "success") {
					$("#delete").html("delete success.");
					return true;

				}
				swal("Poof! Your record has been deleted!", {
					icon: "success",
				});
				setTimeout(location.reload.bind(location), 1000);
			} else {
				swal("Your imaginary file is safe", {
					title: 'Cancelled',
					icon: "error",
				});
			}
		});


	}
</script>