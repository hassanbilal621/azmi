<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="card-alert card " style="background: #262362;">
                        <div class="card-content white-text">
                            <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Company</h5>
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <section class="users-list-wrapper section">
                                <div class="users-list-table">
                                    <!-- datatable start -->
                                    <div class="responsive-table">
                                        <table id="users-list-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Company Id</th>
                                                    <th>Company</th>
                                                    <th>Address</th>
                                                    <th>Status</th>
                                                    <th>edit</th>
                                                    <!-- <th>delete</th>     -->
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php foreach ($allcompanies as $company) : ?>
                                                    <tr>
                                                        <td><?php echo $company['company_id']; ?></td>
                                                        <td><?php echo $company['companyname']; ?></td>
                                                        <td><?php echo $company['companyaddress']; ?></td>
                                                        <?php if ($company['status'] == 'active') { ?>
                                                            <td class="switch">
                                                                <label>
                                                                    <input type="checkbox" checked onclick="disablecompany(this.id)" id="<?php echo $company['company_id']; ?>">
                                                                    <span class="lever"></span>
                                                                </label>
                                                            </td>
                                                        <?php } else { ?>
                                                            <td class="switch">
                                                                <label>
                                                                    <input type="checkbox" onclick="enablecompany(this.id)" id="<?php echo $company['company_id']; ?>">
                                                                    <span class="lever"></span>
                                                                </label>
                                                            </td>
                                                        <?php  } ?>

                                                        <td>
                                                            <a onclick="loadcompanyinfo(this.id)" id="<?php echo $company['company_id']; ?>" type="submit" href="#modal3" name="action">
                                                                <i class="material-icons left">edit</i>
                                                            </a>
                                                        </td>
                                                        <td>

                                                            <!-- <a id="<?php echo $company['company_id']; ?>" onclick="del(this.id)" type="submit" name="action">
                                                                <i class="material-icons left">delete_forever</i>
                                                            </a> -->
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                                </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal3" class="modal">
<div class="modal-content"style="padding: 0;">
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
    function loadcompanyinfo(companyid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_edit_company_adminmodal/" + companyid,
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal3').modal('open');
            }
        });
    }
</script>
<!-- <script>
    function del(id) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            icon: 'warning',
            dangerMode: true,
            buttons: {
                cancel: 'No, Please!',
                delete: 'Yes, Delete It'
            }
        }).then(function(willDelete) {
            if (willDelete) {
                var html = $.ajax({
                    type: "GET",
                    url: "<?php echo base_url(); ?>admin/deletecompany/" + id,
                    // data: info,
                    async: false
                }).responseText;

                if (html == "success") {
                    $("#delete").html("delete success.");
                    return true;

                }
                swal("Poof! Your record has been deleted!", {
                    icon: "success",
                });
                setTimeout(location.reload.bind(location), 1000);
            } else {
                swal("Your imaginary file is safe", {
                    title: 'Cancelled',
                    icon: "error",
                });
            }
        });


    }
</script>
 -->

<script>
    function enablecompany(adminid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/company_enable/" + adminid,
            success: function(data) {
                swal({
                    title: 'Active Company',
                    icon: 'success',
                    timer: 1000,
                }); {
                    setTimeout(location.reload.bind(location), 1500);
                }
            }
        });
    }
</script>
<script>
    function disablecompany(adminid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/company_disable/" + adminid,
            success: function(data) {
                swal({
                    title: 'Deactive Company',
                    icon: 'error',
                    timer: 1000,
                }); {
                    setTimeout(location.reload.bind(location), 1500);
                }
            }
        });
    }
</script>