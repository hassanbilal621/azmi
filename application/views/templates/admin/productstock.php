<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-alert card " style="background: #262362;">
					<div class="card-content white-text">
						<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Stock</h5>
						</h5>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<section class="users-list-wrapper section">
							<div class="users-list-table">
								<div class="responsive-table">
									<table id="page-length-option" class="display">
										<thead>
											<tr>
												<!-- <th></th>
												<th></th> -->
												<th>Product</th>
												<th>Stock</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($products as $product) : ?>
												<td><?php echo $product['product']; ?></td>
												<?php
												$product_id = $product['product_id'];

												$this->db->where('stock_product_id', $product_id);
												$query = $this->db->get('stock');
												$stockdataresults =  $query->result_array();
												$totalupdatestock = 0;
												foreach ($stockdataresults as $stockdataresult) :
													if (isset($stockdataresult['new_stock'])) {
														$totalupdatestock = $totalupdatestock + intval($stockdataresult['new_stock']);
													}

												endforeach;
												?>
												<td><?php echo $totalupdatestock ?></td>
												</tr>
											<?php endforeach; ?>
											</tfoot>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="modal3" class="modal">
	<div class="modal-content">
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
	function loadstockinfo(stockid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_stock/" + stockid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<script>
	function del(id) {

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			icon: 'warning',
			dangerMode: true,
			buttons: {
				cancel: 'No, Please!',
				delete: 'Yes, Delete It'
			}
		}).then(function(willDelete) {
			if (willDelete) {
				var html = $.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>admin/stockdelete/" + id,
					// data: info,
					async: false
				}).responseText;

				if (html == "success") {
					$("#delete").html("delete success.");
					return true;

				}
				swal("Poof! Your record has been deleted!", {
					icon: "success",
				});
				setTimeout(location.reload.bind(location), 1000);
			} else {
				swal("Your imaginary file is safe", {
					title: 'Cancelled',
					icon: "error",
				});
			}
		});


	}
</script>