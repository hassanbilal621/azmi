<div id="main">
	<div class="row">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-alert card " style="background: #262362;">
						<div class="card-content white-text">
							<h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Supplier</h5>
							</h5>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<section class="users-list-wrapper section">
								<div class="users-list-table">
									<div class="responsive-table">
										<table id="users-list-datatable" class="table">
											<thead>
												<tr>
													<th></th>
													<th></th>
													<th>#id</th>
													<th>Supplier name</th>
													<th>Supplier Email</th>
													<th>Image</th>
													<th>Address</th>
													<th>Registration Date</th>
													<th>Edit</th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($suppliers as $supplier) : ?>
													<tr>
														<td></td>
														<td></td>
														<td><?php echo $supplier['suppliers_id']; ?></td>
														<td><?php echo $supplier['suppliers']; ?></td>
														<td><?php echo $supplier['email']; ?></td>
														<td><img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($supplier['supplier_img'])) {
                                                                                       echo $supplier['supplier_img'];
                                                                                    } else {
                                                                                       echo "no-image.png";
                                                                                    }  ?>" width="64px"></td>

														<td><?php echo $supplier['address']; ?></td>
														<td><?php echo $supplier['supplier_date']; ?></td>

														<td>
															<a onclick="loadsupplierinfo(this.id)" id="<?php echo $supplier['suppliers_id']; ?>">
																<i class="material-icons left">edit</i>
															</a>
														</td>
														<td></td>
														<td></td>
													</tr>
												<?php endforeach; ?>
												</tfoot>
										</table>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal3" class="modal">
	<div class="modal-content"style="padding: 0;">
	</div>
</div>
<script>
	function loadsupplierinfo(supplierid) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_supplier_adminmodal/" + supplierid,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<script>
	function del(id) {

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			icon: 'warning',
			dangerMode: true,
			buttons: {
				cancel: 'No, Please!',
				delete: 'Yes, Delete It'
			}
		}).then(function(willDelete) {
			if (willDelete) {
				var html = $.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>admin/suppliersdelete/" + id,
					// data: info,
					async: false
				}).responseText;

				if (html == "success") {
					$("#delete").html("delete success.");
					return true;

				}
				swal("Poof! Your record has been deleted!", {
					icon: "success",
				});
				setTimeout(location.reload.bind(location), 1000);
			} else {
				swal("Your imaginary file is safe", {
					title: 'Cancelled',
					icon: "error",
				});
			}
		});


	}
</script>