<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-alert card " style="background: #262362;">
               <div class="card-content white-text">
                  <h5 class="white-text darken-1" style="font-weight: bold;" class="ml-3">Manage Supplier</h5>
                  </h5>
               </div>
            </div>
            <section class="users-list-wrapper section">
               <div class="users-list-filter">
                  <div class="card-panel">
                     <div class="row">
                        <form>
                           <div class="col s12 m6 l4">
                              <label for="users-list-verified">Company</label>
                              <div class="input-field">
                                 <select class="select2 browser-default" id="users-list-verified">
                                    <option value="">Any</option>

                                    <?php foreach ($allcompanies as $company) : ?>
                                       <option value="<?php echo $company['companyname']; ?>"><?php echo $company['companyname']; ?></option>
                                    <?php endforeach ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col s12 m6 l4">
                              <label for="users-list-role">Customer</label>
                              <div class="input-field">
                                 <select class="select2 browser-default" id="users-list-role">
                                    <option value="">Any</option>

                                    <?php foreach ($customers as $customer) : ?>
                                       <option value="<?php echo $customer['customer_name']; ?>"><?php echo $customer['customer_name']; ?></option>
                                    <?php endforeach ?>

                                 </select>
                              </div>
                           </div>
                           <div class="col s12 m6 l4  ">
                              <label for="users-list-status">Status</label>
                              <div class="input-field">
                                 <select class="select2 browser-default" id="users-list-status">
                                    <option value="">Any</option>
                                    <option value="paid">Paid</option>
                                    <option value="unpaid">Unpaid</option>
                                 </select>
                              </div>
                           </div>

                        </form>
                     </div>
                  </div>
               </div>
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- datatable start -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>
                                       <span>Invoice#</span>
                                    </th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Customer</th>
                                    <th>Tags</th>
                                    <th>Status</th>
                                    <th>invoice</th>
                                    <th>delivey chalan</th>
                                    <th> </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($invoices as $invoice) : ?>
                                    <tr>
                                       <td></td>
                                       <td>
                                          <a href="app-invoice-view.html"><?php echo $invoice['invoice_id']; ?></a>
                                       </td>
                                       <td><span class="invoice-amount"><?php echo $invoice['grand_total']; ?></span></td>
                                       <td><small><?php echo $invoice['invoicedate']; ?></small></td>
                                       <td><span class="invoice-customer"><?php echo $invoice['customer_name']; ?></span></td>
                                       <td>
                                          <small><?php echo $invoice['companyname']; ?></small>
                                       </td>
                                       <?php if ($invoice['invoice_status'] == 'unpaid') { ?>
                                          <td>
                                             <span class="chip lighten-5 red red-text">UNPAID</span>
                                          </td>
                                       <?php } else { ?>
                                          <td>
                                             <span class="chip lighten-5 green green-text">PAID</span>
                                          </td>
                                       <?php       } ?>
                                       <td>
                                          <div class="invoice-action">
                                             <a href="<?php echo base_url(); ?>admin/viewinvoice/<?php echo $invoice['invoice_id']; ?>">
                                                <i class="material-icons">remove_red_eye</i>
                                             </a>
                                       </td>
                                       <td>
                                          <a href="<?php echo base_url(); ?>admin/dc/<?php echo $invoice['invoice_id']; ?>">
                                             <i class="material-icons">insert_drive_file</i>
                                          </a>
                                       </td>
                                       <td></td>
                                    </tr>
                                 <?php endforeach; ?>
                              </tbody>
                           </table>
                        </div>
            </section>
         </div>
      </div>
   </div>
</div>
</div>
</div>