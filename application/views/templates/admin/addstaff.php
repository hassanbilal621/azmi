<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card br-1">
            <div class="card-content">
               <h4 class="card-title">Add Staff</h4>
               <?php echo form_open_multipart('admin/addstaff'); ?>
               <div class="row">
                  <div class="col s2 right">
                     <h6>Date</h6>
                     <input type="text" class="datepicker" name="newsdate" placeholder=" Type Date" required>
                  </div>
               </div>
               <div class="row">
                  <div class="col s12">
                     <div class="row">
                        <div class="col s6">
                           <div class="input-field col s12">
                              <h6>First Name</h6>
                              <input id="name2" type="text" name="firstname">

                           </div>
                           <div class="input-field col s12">
                              <h6>Last Name</h6>

                              <input id="name2" type="text" name="lastname">
                           </div>
                           <div class="input-field col s12">
                              <h6>Phone Number</h6>
                              <input id="number" type="number" name="number">
                           </div>
                        </div>
                        <div class="col s6">
                           <div class="input-field col s12">
                              <div id="file-upload" class="section">
                                 <div class="row section">
                                    <div class="col s12 m12 l12">
                                       <h6>Staff Image</h6>
                                       <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col s12">
                           <div class="input-field col s6">
                              <h6>User Name</h6>
                              <input id="username2" type="text" name="username">
                           </div>
                           <div class="input-field col s6">
                              <h6>Password</h6>
                              <input id="pass" type="password" name="password">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col s12">
                           <div class="input-field col s6">
                              <h6>Cnic</h6>
                              <input id="cnic" type="number" name="cnic">
                           </div>
                           <div class="input-field col s6">
                              <h6>Role</h6>
                              <select class="select2 browser-default" name="role">
                                 <option value="" disabled selected>Role</option>
                                 <option value="2">Manager</option>
                                 <option value="3">User</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col s12">
                           <div class="input-field col s6">
                              <h6>salary</h6>
                              <input id="salary" type="number" name="salary">
                           </div>
                           <div class="input-field col s6">
                              <h6>Finger Print no</h6>
                              <input id="Fingerno" type="number" name="Fingerno">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col s12">
                           <div class="input-field col s12">
                              <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">Save
                                 <i class="material-icons right">save</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
      </div>
   </div>
</div>


<script>
   $(document).ready(function() {
      $('.datepicker').datepicker();
   });
</script>